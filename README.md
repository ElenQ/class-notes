# Courses notes

This repository contains all the notes of the courses that ElenQ Technology
gives.

Every folder contains the notes of that specific course.

Notes, in general, are prepared to be compiled to PDF with [ElenQ Technology's
document templates][elenq-templates], written in Pandoc Flavoured MarkDown.

# LICENSE

CC-BY-SA 4.0 except where otherwise noted.

[elenq-templates]: https://gitlab.com/ElenQ/templates
