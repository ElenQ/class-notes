---
author:         ElenQ Technology
title:          Empresa Digitala
subtitle:       Introducción a Python para el análisis de datos
license:        CC-BY-SA 4.0
changes:        false
changestitle:   Revision history
toc:            true
documentclass:  book
lang:           spanish
links-as-notes: true # Easy printing
---
