# Pandas

## Introduction

Pandas es una biblioteca que aporta a Python estructuras de datos de alto
rendimiento y fácilidad de uso junto con herramientas de análisis de datos.

Licencia BSD e impulsado por NumFOCUS. *Software libre*. *Asegura que no se
abandonará y seguirá mejorandose y permite hacer donaciones.*

Python es un buen lenguaje para extraer datos y prepararlos pero es menos capaz
de analizarlos y modelizarlos. Pandas cubre este hueco. *Ya no es
necesario usar lenguajes específicos como R.*

### Características interesantes de pandas

- Import/Export: CSV, Excel, SQL, HDF5, JSON...
- Alineación de datos y gestión de datos vacíos.
- Pivotaje y cambio de forma de datasets.
- Indexado avanzado, slicing, subsetting etc. *Muy potente*
- Inserción y eliminación de filas y columnas.
- Agregación y transformación con motor de agrupación. *Split-Apply-Combine*
- Merge / Join de alto rendimiento
- Índices jerárquicos (multiíndices)
- Series temporales (*Time series*)
- Muy opitimizado y potente, apartados críticos escritos en *Cython* y *C*.
- Muy usado en infinidad de sectores. *Casos de éxito*

### Herramientas y estructuras de datos

Estructuras de datos rápidas, flexibles, expresivas y estructuradas diseñadas
para trabajar con datos *relacionales* (relational) o *etiquetados* (labeled)
de forma fácil e intuitiva.

High-level pero real-world.

Encaja bien en estos tipos de datos:

- Datos en tablas con columnas de tipos heterogeneos: SQL, Excel...
- Series temporales ordenadas o desordenadas (frecuencia fija no necesaria).
- Datos matriciales con datos etiquetados por filas y columnas.
- Otros

Estructuras primarias:

- Series (unidimensional)
- DataFrame (bidimensional) *Como el `data` de R pero con más funcionalidad*
    - Formado por Series

> Las estructuras de datos de Pandas usan NumPy internamente, que encaja bien
> con otras áreas de la ciencia y la ingeniería. *ScyPy, etc.*

## Tipos de dato

Dos tipos básicos de datos:

1. Series
    - Unidimensional: índice / valor
2. DataFrame
    - Bidimensional: índice - columna / valor
    - Cada columna es un Series, pero todas las columnas tienen un índice
      común.

### Series

Pueden crearse series desde muchos tipos de dato: arrays de numpy,
diccionarios, un escalar mediante una pareja escalar-índice, listas...

``` python
import pandas as pd

s = pd.Series({'a' : 0., 'b' : 1., 'c' : 2.})
s["a"]           # Devuelve 0.
s["d"]           # Lanza la excepción KeyError
s.get("d")       # Devuelve None
s.get("d", 0)    # Devuelve 0
```

Las series tienen nombre (`s.name`), normalmente el de la columna de la que se
extraen, pero pueden renombrarse.

#### Operaciones vectoriales

``` python
s + 2           # Suma 2 a todos los valores, uno por uno
s + s           # Suma los valores uno por uno
                # NOTA: Se alinean automáticamente por etiqueta
```

### DataFrame

Pueden crearse desde muchos tipos de dato, o leerse de fichero: `read_csv`,
`read_json`...

Se comportan como un diccionario de Series y añadir, quitar, etc. se comporta
igual:

``` python
df = DataFrame(datos)
df["a"] = "hola"    # Crea una columna de "hola"s
del df["a"]         # Elimina la columna "a"
```




## Selección

Los datos están indexados, pueden accederse en función del índice: a través de
su etiqueta o a través de su posición.

Muchos modos de acceso, lo importante es aprender la lógica de qué entrega cada
uno:

``` python
df[columna]           # Columna por nombre de columna -> Devuelve una Series
df.loc[etiqueta]      # Fila por etiqueta -> Devuelve una Series
df.iloc[posicion]     # Fila por posición -> Devuelve una Series

df[N:M]               # Filas por slice -> Devuelve un DataFrame
df[bool_vector]       # Filas por vector booleano -> Devuelve un DataFrame
df[callable]          # Filas por *llamable*, cualquier elemento que se pueda
                      # llamar como función y devuelva un indexing válido

df.at[etiqueta, columna]  # Elemento por etiqueta - columna
df.iat[posicion, columna] # Elemento por posición - columna
                          #   `-> Devuelven escalares (Integer, String...)
```

> `iat` y `at` son operaciones más rápidas que `df.loc[etiqueta, columna]`
> porque no analizan el tipo de selector y están optimizadas.

> WARNING: El chaining crea copias del dataset:  
> `df[a][b]` y `df.loc[a, b]` no son equivalentes, a la hora de llenar valores
> hay problemas si se trabaja con *chaining*.

### Filtrado

Mediante un vector booleano:

``` python
df[columna] == 10     # Aplica la comparación por elemento (operación
                      # vectorial) y devuelve una Series con True / False
                            # `-> Un vector booleano!

df[ df[columna] == 10 ]        # Filtra en función del vector booleano
                               # Devuelve la filas que son True en el vector
```

### Indexado

El indexado es mucho más complejo de lo representado más arriba: `loc` e `iloc`
pueden recibir vectores booleanos, listas de etiquetas etc.

Algunas herramientas especiales:

- `.where()`: como un selector normal pero devuelve el dataset completo con
  NaN, None o lo que se le pida en las celdas que no cumplen la condición.

- `.mask()`: opuesto al `.where()`.

- `.query()`: Pide en función de una query en texto. Útil cuando la condición
  es la misma pero hay que ejecutar en varios datasets, así no es necesario
  enviar el propio dataset para obtener el bool vector.  
  `map(lambda x: x.query("a > 10") , [df1, df2, df3])`

#### Multiíndices

Los índices pueden tener diferentes niveles jerárquicos. Aparecen a menudo
cuando se agrupan datos (por mes y año por ejemplo).

Los multiíndices se comportan como un índice normal, pero sus etiquetas son
tuplas con todos los niveles: `df.loc[("nivel1","nivel2")]`. Los niveles pueden
reordenarse.

#### Otros tipos de índices

Existen otros tipos de índices que permiten ordenar los datos de diferentes
maneras: los índices categóricos (`CategoricalIndex`) permiten
repetir valores en el índice, los índices temporales (`DatetimeIndex`) están
optimizados para el uso de fechas y horas, etc.





## Agrupación

Esta consulta SQL:

``` sql
SELECT Column1, Column2, mean(Column3), sum(Column4)
FROM SomeTable
GROUP BY Column1, Column2
```

Selecciona varias columnas, agrupa por `Column1` y `Column2`, y agrega el
resultado obteniendo el `mean` de la `Column3` y el `sum` de la `Column4`,
visualizando además las columnas utilizadas para la agrupación.

Pandas se refiere al `group-by` (agrupar por) como un proceso que cumple
algunos de estos pasos:

- **Split**: Separar los datos en grupos siguiendo algún criterio.
- **Apply**: Aplicar una función a cada grupo de forma independiente.
- **Combine**: Combinar los resultados en una estructura de datos.

El paso **apply** es el más complejo. Pueden hacerse, las siguientes cosas:

- **Agregación**: Obtener resúmenes de los datos, normalmente estadísticos.
  `sum` y `mean` del SQL anterior son ejemplo de esto.
- **Transformación**: Aplicar computaciones específicas de grupo y devolver un
  objeto con el mismo índice. Ejemplo: estandarización de datos de grupo
  (restar la media, zscore, etc.).
- **Filtrado**: Descartar grupos en función de características grupales que
  evalúen a `True` o `False`. Ejemplo: Eliminar grupos con pocos miembros,
  eliminar grupos en función de sus sumatorios...
- **Combinaciones de lo anterior**

No olvidar que los procesos de análisis de datos se basan en estructuras
*map-reduce* (obvian el *filter*).

- Map: Transformación
- Reduce: Agregación
- Filter: Filtrado

Existen muchas formas para generar la agrupación:

- Por función
- Por diccionario o `Series` que mapee etiqueta con nombre de grupo
- Por columna o índice (seleccionando el eje), utilizando `DataFrame`s
- Una lista de todo lo anterior

> NOTA:  
> `df.groupby("columna")` es *sintactic-sugar* de `df.groupby(df["columna"])`

El proceso de agrupación sólo necesita un nombre de grupo para generar las
agrupaciones, por ejemplo:

``` python
def by_vowel(letter):
    """
    This grouping function must return the group identifier
    """
    if letter.lower() in "aeiou":
        return "vowel"
    return "consonant"

grouped = df.groupby(by_vowel)
```

### Acceso a los grupos

`groupby` es una acción *lazy*. Los grupos no se realizan hasta que se accede a
ellos, simplemente se guardan y se entregan cuando se acceda.

``` python
grouped = df.groupby(by_vowel)

vowels = grouped.get_group("vowel")     # Aquí se evalúa el grupo "vowel"
```

Se puede iterar sobre los grupos:

``` python
for name, group in grouped:
    # Cuerpo
```

> NOTA: Si se ha agrupado por varios criterios, el nombre del grupo será una
> tupla con todos los nombres de grupo.

### Agregación

``` python
import numpy as np

grouped.agg(np.mean)                     # `agg` es un alias a `aggregate`
grouped.agg([np.sum, np.mean, np.std])   # Varias funciones por columna
grouped.agg({
    "column1": np.sum,
    "column2": lambda x: np.std(x, ddef=1)
})                      # Funciones diferentes en cada columna
```

#### Funciones optimizadas

Las funciones `sum`, `mean`, `std`, y `sem` están optimizadas con Cython. **Son
más rápidas que implementadas en python plano**.

``` python
grouped.sum()

# Es equivalente a:
grouped.agg(np.sum)     # Pero esta implementación es más lenta
```

### Transformación

``` python
grouped.transform( lambda x: x.mean() - x.max() )
```

### Filtrado

``` python
grouped.filter( lambda x: len(x) > 2 )  # Descarta los grupos de menos de 2
                                        # elementos
```

### Aplicación de funciones arbitrarias

Para operaciones que requieran temas más extraños se puede utilizar `apply`, al
igual que en un `DataFrame` completo. Esta función es mucho más flexible que
cualquiera de las mencionadas y puede realizar filtrados, agregaciones, etc.
en función de sus argumentos de entrada.

> WARNING: la implementación actual ejecuta dos veces el primer elemento por
> motivos de optimización. Esto puede sorprender al usuario con un
> comportamiento inesperado.



## Limitaciones y mejoras posibles

La principal carencia de pandas radica en su propio diseño. Pandas está
diseñado para cargar los datos en la memoria del ordenador y operar sobre ella.
No soporta *clustering* por lo que la única memoria a lo que tiene acceso es a
la de un único equipo, limitando la cantidad de datos que pueden tratarse.

Otras herramientas como SAS funcionan sobre disco duro, permitiendo atacar
mayor cantidad de datos sacrificando velocidad de acceso.

Otras herramientas de clustering o paralelismo aportan la posibilidad de
orquestar varios equipos de modo que se combina la memoria de todos ellos.
Además, el procesamiento en paralelo acelera los cálculos de forma radical.

La potencia de pandas está en su flexibilidad, velocidad de cálculo y simpleza
y además en su rico ecosistema que solventa, en gran medida, las limitaciones
mencionadas y alguna otra.





## Ecosistema pandas

### Statsmodels

[http://www.statsmodels.org/stable/index.html](http://www.statsmodels.org/stable/index.html)

Una librería de econometría para python. Utiliza pandas internamente para
portar una potente API de econometría para python que soporta infinidad de
estadísticas más allá de lo que puede aportar pandas.

### Sklearn-pandas

[https://github.com/scikit-learn-contrib/sklearn-pandas](https://github.com/scikit-learn-contrib/sklearn-pandas)

Un puente entre los modelos de Machine Learning de ScikitLearn y los modelos de
datos de pandas. ScikitLearn es una librería de machine learning muy utilizada
en python de la que se habla en el siguiente bloque.

### Dask

[https://dask.readthedocs.io/en/latest/](https://dask.readthedocs.io/en/latest/)

Un conjunto de herramientas de clustering y de análisis de volúmenes de datos
mayores que la memoria. Suple ambas de las limitaciones de pandas mencionadas
en el apartado anterior. Dask reimplementa tipos básicos de python y
DataFrames y Series de pandas para aportar funcionalidades avanzadas
(clustering, datasets en disco, etc). Pertenece al ecosistema Blaze.

### Blaze

[http://blaze.pydata.org/](http://blaze.pydata.org/)

Blaze es una interfaz que permite acceder a grandes cantidades de datos desde
Python traduciendo sintaxis del estilo pandas a diferentes sistemas de
computación de datos. Además de ser una librería en sí misma, agrupa varias
librerías diferentes, como Dask, formando un ecosistema de análisis de datos
para entornos Big Data.

### Bokeh y Seaborn

[https://bokeh.pydata.org/en/latest/](https://bokeh.pydata.org/en/latest/)  
[https://seaborn.pydata.org/](https://seaborn.pydata.org/)

Bokeh y Seaborn son dos librerías de visualización diferentes a la librería
estándar de pandas para visualización (matplotlib) que aportan gráficos más
potentes y agradables para la vista. Existen otras, cada una con sus
peculiaridades y puntos fuertes.



## Más información sobre Pandas

Principalmente se recomienda profundizar en páginas de la documentación de
pandas, porque tiene una calidad excelente. Además de una descripción completa
de toda la API de pandas, existe un manual paso a paso lleno de ejemplos de
donde se ha extraído gran parte del contenido de este curso. Algunas partes han
quedado fuera por falta de tiempo y otras es interesante trabajarlas de forma
individual. Aquí un pequeño listado:

- [Tutorials](https://pandas.pydata.org/pandas-docs/stable/tutorials.html):
  Un conjunto de explicaciones y ejemplos para entender lo básico de pandas.
  Los ejemplos del apartado
  [*cookbook*](https://pandas.pydata.org/pandas-docs/stable/tutorials.html#pandas-cookbook)
  han sido explicados y desarrollados por la genial programadora Julia Evans.
  Merecen la pena para refrescar los conceptos principales de pandas.

- [Merge, join and
  concatenate](https://pandas.pydata.org/pandas-docs/stable/merging.html):
  Combinación, cruzado y concatenado de datasets. Explica cómo unir varios
  `DataFrame` en uno único. Aplica los conceptos de indexado que se han tratado
  antes.

- [Visualization](https://pandas.pydata.org/pandas-docs/stable/visualization.html):
  Analiza las diferentes funciones de pandas para visualizar gráficos mediante
  la librería `matplotlib`.

- [Reshaping and Pivot Tables](https://pandas.pydata.org/pandas-docs/stable/visualization.html):
  Enseña las funcionalidades de pandas para el pivotaje de tablas y
  reestructuración de datos. Es necesario e importante en el uso avanzado.

Otras páginas del manual analizan casos de uso específicos como el uso de
series temporales. Se recomienda una lectura a la hora de utilizar este tipo de
herramientas.
