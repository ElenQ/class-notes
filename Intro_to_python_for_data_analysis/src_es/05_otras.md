# Otras herramientas

Todas las herramientas y librerías mencionadas aquí son Software Libre, su
código puede consultarse y son de libre uso. Cada una tiene su propia licencia
de software, antes de seleccionar herramientas se recomienda analizar las
licencias y comprobar si son compatibles con el proyecto a realizar.

## Cluster Computing: Apache-Spark

Apache-Spark es un framework de *cluster-computing* que aporta una interfaz
para programar *clusters* de equipos con paralelismo implícito y tolerancia a
fallos.

Aunque está hecho en el lenguaje de programación Scala dispone de bindings para
python llamados *pySpark*.

Se basa en el concepto RDD (Resilient Distributed Dataset).

Se trabaja mediante una estrategia *map-reduce* y con combinaciones por clave,
etc. Típico en entornos Big Data.

También dispone de herramientas SQL y Machine Learning en su ecosistema.

> Fue desarrollado por la Universidad de Berkeley y después donado a la Apache
> Foundation para que continuara con el desarrollo.

## Machine Learning: Scikit-Learn

Scikit-Learn es una librería de Machine Learning para python diseñada para
interoperar con las librerías científicas típicas de python: Numpy y Scipy.

Como en pandas, muchos de sus módulos están escritos en Cython para aportar
mayor velocidad de cálculo.

## Procesamiento de lenguaje natural: NLTK y SpaCy

Ambas son bibliotecas de procesado de lenguaje natural para Python. NLTK está
diseñada para trabajar únicamente en Inglés y SpaCy soporta otros idiomas
(Alemán, Español, Francés...).

NLTK centra su uso en la enseñanza y la investigación, mientras que SpaCy está
diseñada para trabajar en software de producción.

## Matemática simbólica y redes neuronales: TensorFlow

TensorFlow es una librería de *dataflow programming* diseñada por Google para
sus herramientas internas. Es una librería de matemática simbólica muy usada en
Machine Learning, sobre todo para crear redes neuronales.

Es común encontrar proyectos que la utilizan para detectar objetos en
fotografías o vídeo.

## Web Crawling: Scrapy

Scrapy es un framework de *web crawling*. Tiene un enfoque de alto nivel, es
sencillo de usar y rápido. Está diseñada de forma asíncrona (siguiendo la
estela de Twisted, una de sus dependencias), algo poco común en python pero que
aporta muchas ventajas en este entorno.

> La librería `BeautifulSoup4` también es muy común para procesar el contenido
> de las páginas, junto con `requests` puede ser otra opción para proyectos de
> web crawling menos exigentes.

## Automatización de navegador web: Selenium

Selenium es un framework de testing de aplicaciones web con bindings para
muchos lenguajes diferentes, entre ellos python. Además de para testear
aplicaciones web es muy usado para realizar *web scraping* en páginas que
necesiten de JavaScript.
