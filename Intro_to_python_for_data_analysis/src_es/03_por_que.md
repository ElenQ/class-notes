# Análisis de datos ¿Por qué python?

Python es un lenguaje de **uso general**. Algunas de las características de
Python:

- Flexibilidad
    - Multiparadigma. *Facilita arquitecturas variadas*
- Sencillez (de aprendizaje y uso)
- Eficiente
    - Fácil de escribir y conciso. *Eficiente en la escritura*
    - Fácilmente extensible. *Evita la sobreingeniería, directo al grano*
    - Extensible con módulos en C
- Alto nivel de abstracción
- Extensa librería estándar

## Beneficios en el análisis de datos

- Extensa librería estándar
    - Matemática avanzada
    - Input/Output
- Multiparadigma
    - Soporta programación funcional. Encaja bien con el proceso de análisis de
      datos. *Map-Reduce*
    - Fácil de migrar.
- Conexión con las matemáticas. *Fácil de aprender por estadistas*
- Alto nivel de abstracción. *Elimina los pormenores técnicos del proceso*
- Uso general. *Muy usado en otros temas como el desarrollo web, scripting,
  desarrollo embebido... Encaja bien con otros procesos. Probablemente ya esté
  en uso en la arquitectura actual. Casi todos los desarrolladores ya lo
  conocen.*
- Ecosistema rico.

