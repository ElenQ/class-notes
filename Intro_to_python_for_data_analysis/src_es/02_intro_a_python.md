# Introducción a Python

> Python is an interpreted high-level programming language for general-purpose
> programming. Created by Guido van Rossum and first released in 1991, Python
> has a design philosophy that emphasizes code readability, and a syntax that
> allows programmers to express concepts in fewer lines of code, notably using
> significant whitespace. It provides constructs that enable clear programming
> on both small and large scales.
>
> Python features a dynamic type system and automatic memory management. It
> supports multiple programming paradigms, including object-oriented,
> imperative, functional and procedural, and has a large and comprehensive
> standard library.
>
> — Wikipedia

### REPL (Read, Eval, Print and Loop)

La REPL lee (*read*) el código mientras lo escribes, lo evalúa (*eval*),
muestra el resultado (*print*) y de nuevo vuelve a empezar (*loop*). Por eso se
llama REPL.

Todas las piezas de código fuente de estos ejemplos están ejecutadas en la
REPL. La REPL se puede arrancar con el comando `python` en la terminal.

### Filosofía

```

>>> import this
The Zen of Python, by Tim Peters

Beautiful is better than ugly.
Explicit is better than implicit.
Simple is better than complex.
Complex is better than complicated.
Flat is better than nested.
Sparse is better than dense.
Readability counts.
Special cases aren't special enough to break the rules.
Although practicality beats purity.
Errors should never pass silently.
Unless explicitly silenced.
In the face of ambiguity, refuse the temptation to guess.
There should be one-- and preferably only one --obvious way to do it.
Although that way may not be obvious at first unless you're Dutch.
Now is better than never.
Although never is often better than *right* now.
If the implementation is hard to explain, it's a bad idea.
If the implementation is easy to explain, it may be a good idea.
Namespaces are one honking great idea -- let's do more of those!

```

## Sintaxis general

Muy simple.

- Salto de línea indica final del commando
- Los bloques de código se definen mediante la *indentación*. Tomarse esto muy
  en serio.

### Comentarios

Cualquier cosa más allá de un `#` se considera un comentario y el intérprete lo
ignora.


## Variables y tipos

El operador `=` enlaza (*bind*) un nombre a un valor.

### Tipos básicos

Los tipos básicos son **inmutables**.

#### Integer

Números enteros. Positivo o negativo.

``` python
>>> a = 10
>>> a
10
```

#### Float

Números de coma flotante. Positivos o negativos.

``` python
>>> a = 1.23324
>>> a
1.23324
```

#### String

El delimitador puede ser `"` o `'`.

``` python
>>> a = "String"
>>> a
'String'
```
> No hay `char`, son simplemente Strings de longitud 1.

#### Boolean

`True` o `False`.

``` python
>>> a = True
>>> a
True
```

### Tipos complejos

#### Tuple

Las tuplas son una colección de valores. Son **inmutables**.

``` python
>>> a = ('1', 23, -12.3232)
>>> a
('1', 23, -12.3232)
>>> a[0]
'1'
>>> a[1]
23
>>> a[2]
-12.3232
>>> a[-1]
-12.3232
```

#### List

Como una tupla pero **mutable**. Peor rendimiento pero más funcionalidades.

``` python
>>> a = ['1', 23, -12.3232]
>>> a
['1', 23, -12.3232]
>>> a.append("extra")
>>> a
['1', 23, -12.3232, 'extra']
```

#### Dictionary


Como una lista pero con parejas clave-valor. Son **mutables**.

> Las parejas clave-valor no se guardan en orden, si se requiere un orden es
> necesario usar OrderedDict o alguna clase similar.

``` python
 a = {"first": '1', "second": 23, "third":-12.3232}
>>> a
{'second': 23, 'first': '1', 'third': -12.3232}
>>> a["third"]
-12.3232
>>> a["fourth"] = 2
>>> a
{'second': 23, 'first': '1', 'third': -12.3232, 'fourth': 2}
```

### Todo es una referencia

``` python
>>> a = []
>>> b = a
>>> b.append(1)
>>> a
[1]
>>> b
[1]
```

No hay problema con tipos *inmutables* porque no pueden cambiar. La
mutabilidad es peligrosa.

``` python
>>> a = []
>>> b = a.copy() # Make a shallow copy
>>> a.append(1)
>>> a
[1]
>>> b
[]
```

## Control de flujo

### Condicionales

Los condicionales separan el flujo de ejecución en función de una condición.

#### if

``` python
if condition1:
    # body for condition1
    # This is only processed if condition1 is evaluated to True
elif condition2:
    # body for condition2
...
elif conditionN:
    # body for conditionN
else:
    # body for others
```

Los bloques `elif` y `else` son opcionales.

Las condiciones tienen que evaluar a `True` o a `False`.

Existen valores *Truthy* y *Falsey*: [valores diferentes que se resuelven como
verdadero o falso](https://docs.python.org/3/library/stdtypes.html#truth-value-testing)


### Bucles

Los bucles repiten bloques de código en función de una condición.

#### while

``` python
while a < 10:
    # will be repeated until `a` is greater or equal than 10
```

Repite el bloque de código *mientras que* la condición se cumpla.

#### for

El **for** itera por los elementos de una secuencia \*.

``` python
for i in [1,2,3,4]:
    # will be repeated four times changing `i`'s value with the
    # contents of the array
    if i == 4:
        break
else:
    # This block happens when the previous block doesn't end with a `break`
    # Else is optional.
    # For this example it's not executed. *See next section*.
```

_\*Listas, Tuplas, Strings y Diccionarios se comportan como secuencias, aunque
hay más. En el apartado de Programación Orientada a Objetos se explica por
qué._


#### break-ing, continue-ing and pass-ing

Algunas sentencias que afectan a los bucles:

- `break`: Rompe el bucle actual y la ejecución continúa después del bucle.
- `continue`: Salta la ejecución actual del bucle y reevalúa la condición para
  seguir ejecutándolo o salir.
- `pass`: No hace nada. En `python` es necesario llenar los bloques que esperan
  una indentación, si aún no se ha desarrollado el bloque y se deja vacío el
  intérprete no funcionara por encontrarse un error de sintaxis. Poniendo
  `pass` se evita el error.

## Operadores

``` python
>>> 1 + 2
3
>>> 1 == 2
False
```

### Comprobación de verdad

Hay infinidad de operadores de comprobación. Son útiles en bucles y
condicionales. Siempre evalúan a `True` o `False`.
[link](https://docs.python.org/3/library/stdtypes.html#truth-value-testing)

| operator | meaning |
|---|---|
| <  | strictly less than |
| <=  | less than or equal |
| >  | strictly greater than |
| >=  | greater than or equal |
| ==  | equal |
| !=  | not equal |
| is  | object identity |
| is not  | negated object identity |
| in  | containment test |
| not in  | negated containment test |

### Matemáticas

| operator | meaning |
|---|---|
| + | addition |
| - | subtraction or negative |
| `*` | multiplication |
| / | division |
| `**` | power |
| % | remainder |

## Funciones

Las funciones son piezas de código reunidas para simplificar el trabajo y
**potenciar la reutilización de código fuente**.

``` python
def fib(num):
    """
    This is a documentation block called `docstring`. It's optional.
    Read it with `help(fib)`.
    """
    res = []
    a, b = 0, 1
    while a < num:
        res.append(a)
        a, b = b, a+b
    return res              # exit with `res` value

>>> fib(10)
[0, 1, 1, 2, 3, 5, 8]
>>> result = fib(40)
>>> result
[0, 1, 1, 2, 3, 5, 8, 13, 21, 34]
```

Las funciones son un tipo más de variables y se pueden tratar como tal.

``` python
>>> fib
<function fib at 0x7fd008eb2578>
```

_\* [Más información](https://docs.python.org/3.6/tutorial/controlflow.html#more-on-defining-functions)_

### Memoria y *Scope*

Python tiene un recolector de basura (*garbage-collector*) que controla la
memoria automáticamente eliminando las variables que no están en uso.

El *scope* es la pieza de código donde una variable es accesible.

La regla simple del scope es:

> El scope va del nivel de indentación actual a los niveles más profundos.


Por ejemplo:

``` python
>>> def test1():
...     a = 1     # Local variable declaration, only accessible here
...
>>> def test2():
...     return a  # Not declared, a is out of scope
...
>>> test2()
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "<stdin>", line 2, in test2
NameError: global name 'a' is not defined
```

Pero:

``` python
>>> a = 10
>>> def test2():
...     return a  # `a` is declared in the upper level, it has access
...
>>> test2()
10
```

#### Closures

Las funciones guardan una copia de su contexto cuando son declaradas.

``` python
def func_creator():
    a = 10                  # local variable
    def incrementer( arg ):
        return arg + a      # `a` is in scope
    return incrementer

>>> inc = func_creator()
>>> inc
<function incrementer at 0x7f52f8947668>
>>> inc(10)                    # incrementer function remembers `a`
20
>>> a                          # Not in scope!
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'a' is not defined
```

#### lambda

Las lambdas son funciones sin nombre, se comportan como una función normal pero
hay que escribir menos código.

``` python
>>> inc = lambda x: x+1
>>> inc(1)
2
>>> inc(10)
11
>>> inc
<function <lambda> at 0x7f52f8947758>
```

## Archivos

``` python
>>> f = open('src/contenido.md', 'r')     # `r` is the mode
>>> f.read()[0:100]
'# Python 3\n\n## Intro\n\n> Python is an interpreted high-level ...'
>>> f.close()
```

Más elegante con la sentencia `with`:

``` python
>>> with open('src/contenido.md', 'r') as f:
...     f.read()[0:100]
...
'# Python 3\n\n## Intro\n\n> Python is an interpreted high-level ...'
```

## Módulos y paquetes

Un módulo (*module*) de Python es simplemente un archivo de código fuente que
puede exponer clases, funciones y variables globales.

Un paquete (*package*) de Python es un directorio de módulo(s).

En los directorios se añade un archivo `__init__.py` para indicar a python que
ese directorio es un paquete.

> Aunque se puede introducir código en el `__init__.py`, es muy común que esté
> vacío.

### Cargar un módulo

``` python
>>> import math      # imports math and leaves it in `math` namespace
>>> math.pow(2, 4)   # `math` prefix is needed
16.0
>>> from math import pow     # from `math` only import pow to the local scope
>>> pow(2, 4)                # no prefix needed
16.0
>>> from math import pow as p  # like before but with a different name
>>> p(2,4)
16.0
```

Los paquetes pueden tener subcarpetas que se tratan como namespaces:
`import package.module` o `import package.subpackage.module`.

#### Search path

La variable `sys.path` contiene los paths en los que se buscan módulos.

Por defecto:

  - El directorio del fichero en ejecución o el directorio donde se ha
    ejecutado la REPL.
  - PYTHONPATH: una variable de entorno con los directorios de búsqueda.
  - Unos directorios por defecto dependientes de la instalación.

### Ejecutar un módulo

Ejecutando: `python modulename.py [args]` en la línea de comandos del sistema.
Esto ejecuta el módulo con la variable `__name__` con valor `__main__`.

``` python
if __name__ == "__main__":
    import sys                # This block won't run if the module is imported
    fib(int(sys.argv[1]))     # but it will when it's executed directly
```

### Herramientas

Python dispone de varias herramientas para la gestión de dependencias y la
instalación de paquetes:
`pip`, `easy_install`...

Y algunas herramientas para separar entornos de desarrollo:
`virtualenv-wrapper`, `pipenv`...


## OOP (Object Oriented Programming)

``` python
class Dog:

    kind = 'canine'         # class variable shared by all instances

    def __init__(self, name):
        self.name = name    # instance variable unique to each instance
    def bark_name(self):    # this is a class method
        print('Wooof' + self.name) # `self` is used to access the instance

>>> d = Dog('Fido')         # create a new instance named Fido
>>> e = Dog('Buddy')        # create a new instance named Buddy
>>> d.kind                  # shared by all dogs
'canine'
>>> e.kind                  # shared by all dogs
'canine'
>>> d.name                  # unique to d
'Fido'
>>> e.name                  # unique to e
'Buddy'
>>> e.bark_name()
'Wooof Buddy'
```

### Herencia (*inheritance*)

Las clases pueden heredarse: `class Dog(Animal)`

Esto crearía una nueva clase `Dog` con todos los métodos y propiedades de la
clase `Animal`. Si la clase `Dog` definiera algo de nuevo, pisaría a lo
definido en la clase `Animal`.

### Interfaces

Una interfaz es un conjunto de métodos que una clase debe tener para
interactuar de un modo estándar con el exterior.

Por ejemplo, la interfaz iterador define qué métodos son necesarios para actuar
como un iterable.

Un iterador:

``` python
class Counter:
    def __init__(self, low, high):
        self.current = low
        self.high = high

    def __iter__(self):
        return self

    def __next__(self):
        if self.current > self.high:
            raise StopIteration
        else:
            self.current += 1
            return self.current - 1

>>> for c in Counter(3, 8): # Prints 3,4,5,6,7,8
>>>    print(c)
```

## Más información

- [The python tutorial](https://docs.python.org/3.6/tutorial/index.html)
- [The hitchhiker's guide to python!](http://docs.python-guide.org/en/latest/)
- [Python tips (*Avanzado*)](http://book.pythontips.com/en/latest/index.html)
