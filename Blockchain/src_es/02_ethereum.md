# Ethereum

## Por qué Ethereum

Ethereum nace de las ideas de Bitcoin pero con otro objetivo: crear un
ordenador global con un estado compartido. Aprovechando los conceptos técnicos
que Bitcoin resuelve, Ethereum construye una generalización técnica más allá de
una criptomoneda, permitiendo la ejecución de programas arbitrarios de forma
descentralizada.

Hoy en día Ethereum es la infraestructura para muchas aplicaciones distribuidas
y su Blockchain está creciendo de forma radical. Así como Bitcoin fue el primer
caso de uso del uso del Blockchain y del consenso distribuido, Ethereum es el
fundamento de muchos de los conceptos de moda (*smart contracts*, *DApps*...)
que es necesario conocer para mantener un espíritu crítico y para disponer de
herramientas a la hora de desarrollar nuestras propias aplicaciones
distribuidas, basadas o no en un Blockchain.

## Qué es Ethereum

- Un *world computer*, un ordenador global que:
    - Se comporta como una *máquina de estados global*, al igual que Bitcoin,
      pero con una cantidad de estados prácticamente infinito.
    - Dispone de una *máquina virtual* que se encarga de transicionar de un
      estado a otro.
        - La máquina virtual ejecuta *programas turing complete*, conocidos como
          *smart contract*, que generan estos cambios.
        - La *máquina virtual* opera como un *singleton* global, para que
          parezca que es la misma máquina ejecutándose en todas partes.

- Ethereum **no es** una criptomoneda, **usa** una criptomoneda para
  asegurar su funcionamiento. Sirve para pagar por la ejecución de los
  programas.

## Funcionamiento general

> NOTA: El funcionamiento general de Ethereum está descrito en comparación con
> Bitcoin y el reciente estado del arte en el White Paper de Ethereum.
> [https://github.com/ethereum/wiki/wiki/White-Paper](https://github.com/ethereum/wiki/wiki/White-Paper)
>
> Las decisiones de diseño tomadas en Ethereum pueden consultarse aquí:
> [https://github.com/ethereum/wiki/wiki/Design-Rationale](https://github.com/ethereum/wiki/wiki/Design-Rationale)

Ethereum puede funcionar como una criptomoneda, pero es mucho más que eso. La
moneda existe para pagar por la ejecución de los *smart contracts*.

Los *smart contract* se ejecutan en una máquina virtual (*Ethereum Virtual
Machine*, EVM) que opera como un *singleton global*.

Cada *nodo* de la red descentralizada de Ethereum dispone de una máquina
virtual de Ethereum (*EVM*) y un Blockchain. El nodo ejecuta los *smart
contracts* en la máquina virtual y guarda el estado resultante.

Ethereum se diferencia de Bitcoin en que está orientado a crear una cuenta.
Esto se refleja más tarde en su propio comportamiento, pero principalmente
genera una diferenciación inicial: Existen dos tipos de cuentas:

- Las que hacen referencia a un contrato (*contract account*)
- Las que hacen referencia a un usuario (*Externally Owned Account* o *EOA*)

La moneda *Ethereum* puede enviarse de un usuario a otro, de una cuenta *EOA* a
otra. Sin embargo, al enviar moneda a una *contract account*, el contrato se
ejecutará.

Los contratos no pueden ejecutarse por sí mismos, necesitan de un usuario que
los dispare, aunque sí que pueden ejecutar otros contratos una vez que han sido
disparados.

Uno de los detalles de seguridad de Bitcoin provoca que su *script* sea un
lenguaje no *turing complete*. Ese detalle hace que los nodos no puedan
quedarse encerrados en la ejecución de un script, ya que no es posible que
éstos tengan bucles infinitos. En Ethereum es posible que los scripts, ahora
*smart contracts*, tengan bucles infinitos.

Los *smart contracts* tienen un mecanismo para limitar su ejecución. Cada
transacción añade el concepto de *gas*, que es la *gasolina* necesaria para
procesar la transacción. Si la transacción dispara un *smart contract* la
ejecución consumirá *gas* hasta que la ejecución termine o el *gas* se agote.

## Clientes de Ethereum

Bitcoin no está definido de forma formal, sino que existe una implementación de
referencia: *Bitcoin Core*. Ethereum está definido en un paper conocido como
*Yellow Paper*.

> NOTA: Existe también una versión para todos los públicos llamado *Beige
> Paper* mantenido por la comunidad.

### Full Nodes

Los nodos completos guardan el blockchain completo y disponen de un *Ethereum
Virtual Machine* para ejecutar los contratos.

Hoy en día los nodos completos tienen unos requerimientos de recursos
difícilmente alcanzables por usuarios individuales:

- Blockchain tiene un tamaño superior a 1TB.
- Requiere de una velocidad de escritura demasiado elevada para utilizar discos
  duros convencionales, necesita discos de estado sólido.

### Remote Ethereum Clients

Similar a los nodos SPV de Bitcoin, para evitar la carga de un nodo completo,
existen nodos ligeros que dependen de un nodo completo pero aportan
funcionalidades similares. En Ethereum existen varios tipos, algunos sólo
sirven como cartera, otros navegan DApps, etc.



## Claves y direcciones

Hace un uso criptográfico muy similar a Bitcoin.

- Usa la misma curva elíptica.
- $k$ de 256 bits $\rightarrow$ $K = k \cdot G$

### Función de hashing

- Bitcoin usa SHA-256
- Ethereum usa Keccak-256

> NOTA: Keccak es una familia de funciones de hashing. Una de ellas participó
> en el concurso SHA-3, ganando el concurso. Muchas veces se hace referencia a
> Keccak-256 con el nombre SHA-3, pero no son la misma función aunque
> pertenecen a la misma familia. **No son equivalentes**.

### Direcciones

$$
A = 20LSBytes( Keccak256( K ) )
$$

Las direcciones se obtienen de los 20 bytes menos significativos del hash de la
clave pública.

Se representan en Hexadecimal, indicando 0x como prefijo.

Originalmente no se representaban con ningún tipo de código detector de
errores, porque se pretendía crear una capa de abstracción que ocultase las
direcciones al usuario con un formato compatible con IBAN llamado ICAP. Este
formato no se ha adoptado por lo que se diseña un formato de checksum basado en
la capitalización de los caracteres: EIP-55

#### Mixed-case checksum address encoding: EIP-55

Usa la capitalización de los caracteres alfabéticos de la representación
hexadecimal como código detector de errores.

- Los clientes que no lo soporten pueden ignorar la capitalización de forma
  sencilla.
- La dirección se representa del mismo modo.

Funciona obteniendo el hash keccak-256 de la dirección y comparando el valor de
las primeras posiciones del hash con la propia dirección. En los casos en los
que el valor del cuarteto del hash sea mayor o igual que 8 (en hexadecimal), el
cuarteto con la posición correspondiente en la dirección se representará en
mayúsculas, en caso contrario se representará en minúsculas.


### Carteras

Las carteras pueden referirse a diferentes conceptos en Ethereum.

La perspectiva del desarrollador es que las carteras controlan las claves del
usuario. Algunas saben interactuar con *smart contracts*, etc.

La tecnología es **la misma** que en Bitcoin: *Key-stretching*, mnemónicos,
etc. Usa los mismos BIP.

> NOTA: Muchas carteras, como MyEtherWallet utilizan un *keystore file* que es
> una clave privada codificada en formato JSON con unos metadatos adicionales.



## Transacciones

Son mensajes firmados y originados por una EOA.

Las transacciones suponen un *tick* en el estado global, sólo las transacciones
generan cambios.

### Estructura de las transacciones

- Nonce
- Gas price
- Gas limit
- Recipient
- Value
- Data
- v,r,s

Las transacciones se serializan con RLP (Recursive Length Prefix), un formato
Big Endian que asegura que las longitudes son múltiplos de ocho.

No existe *from* se deduce de los valores *v*, *r* y *s*.

#### Nonce

El *nonce* es el número de transacciones válidas hechas con esta dirección.
Sirve para no repetir transacciones y asegurar un orden:

- No hay sistema de UTXO como en Bitcoin, es necesario ordenar las
  transacciones.
- Está orientado a cuentas, no a transacciones.

El *nonce* no se almacena en el Blockchain, se deduce contando el número de
transacciones del Blockchain.

> NOTA: Es necesario ser cuidadoso con el *nonce*: Deben ser correlativos. Si
> no son correlativos el Blockchain esperará a que lleguen las transacciones
> anteriores. **Pueden bloquearse transacciones**.
>
> Hay que ser cuidadoso en entornos multithread. Todos los threads necesitan
> saber cuál es el número de transacción a registrar.

#### Gas

El *gas* es una moneda especial para pagar los costes de la ejecución. Es una
moneda diferente al Ether, hay que convertirlo.

##### GasPrice

El *gasPrice* es el precio que se pagará por el *gas*. A mayor precio mayor
incentivo, por tanto mayor prioridad en la transacción.

##### GasLimit

El *gasLimit* es la cantidad máxima de gas a pagar por el registro de la
transacción. Por tanto:

$$
Total\ to\ pay = gasPrice \cdot gasLimit
$$

En transacciones simples EOA a EOA la cantidad de gas que consume es de
$21000$. Otras transacciones que activan un *smart contract* son impredecibles
debido a la complejidad de éstos. Se calcula un límite de gas y si el contrato
no gasta todo lo devuelve.

#### Recipient

La dirección de Ethereum de la persona (EOA) o contrato al que se le envía la
transacción.

> WARNING: No se valida

#### Value y Data

Estos campos forman el payload de las transacciones. Ambos son opcionales:

- Con sólo *value* son pagos
- Con sólo *data* son invocaciones
- Con ambas son pagos e invocaciones
- Las transacciones sin ninguno de los campos son posibles pero aún no sirven
  para nada

##### Pagos

- A EOAs: se registra como un cambio de estado en el Blockchain
- A contratos: La EVM ejecuta el contrato y trata de ejecutar la función
  indicada en los datos. Si no hay función, se ejecuta la función de *fallback*.
  Si no hay función de *fallback* incrementa los fondos del contrato como si
  fuera un EOA.
    - El contrato puede rechazar pagos
    - Si termina sin lanzar alguna excepción, acepta los fondos

##### Enviando datos

- A EOAs: se pueden enviar datos a EOAs, pero no tiene uso aún
- A contratos: se interpreta como una invocación del contrato. Los datos son
  una serialización HEX de:
    - Selector de función: los 4 primeros bytes del keccak-256 del prototipo de
      la función a llamar
    - Argumentos para la ejecución de la función

##### Transacción especial: Creación de contrato

- Se envían a una dirección especial: zero address (`0x0...0`)
- La zero address no representa ni un contrato ni una EOA. **Sólo sirve para
  esto**.
- La creación del contrato sólo recibe el contrato compilado (a *bytecode*)
  como datos de entrada. En *solidity*: `solc -bin contrato.sol`

#### Firma digital (v,r,s)

En la implementación de ECDSA de Ethereum el mensaje es la transacción (su
hash) y la clave de firma es la clave privada del EOA.

$$
Sig = F_{sig}( Keccak256( m ), k )
$$

Donde:  
$k$ = Clave privada  
$m$ = La propia transacción codificada en RLP  
$Sig$ = Resultado de la firma, que entrega dos valores: (r, s)  

El valor $v$ indica dos cosas:

1. Chain ID: que sirve para que las transacciones no se repitan de un
   Blockchain a otro.
2. Recovery indentifier: Indica la paridad de R.

##### Public key recovery

No existe *from* en las transacciones, se obtiene de la firma con un proceso
llamado *public key recovery*.

$r$, como la coordenada $x$ de la firma, representa dos puntos en la curva
elíptica: $R$ y $R'$, por tanto:

$$
K = r^{-1}(s \cdot R - z \cdot G)
$$
$$
K' = r^{-1}(s \cdot R' - z \cdot G)
$$

Donde:  
$z$ = los $n$ menores bits del hash del mensaje, donde $n$ es el orden de la
curva elíptica  

Para conocer la verdadera clave pública, se utiliza $v$ para indicar si su
coordenada $y$ es positiva o negativa.

#### Emisión de transacciones

Ethereum utiliza un protocolo *flood routing* para enviar las transacciones.

La red Ethereum utiliza una red P2P. Cada uno de los nodos de esta red se
conecta a otros nodos, no necesariamente todos, a los que se conoce como
vecinos o *neighbors*. Al recibir una transacción, los nodos de la red la
validan y, si es correcta, la almacenan y envian a sus vecinos. Es imposible
saber si una transacción recibida ha sido creada por el vecino adyacente o si
éste sólo estaba extendiéndola. Esto es clave para aportar privacidad.

#### Registro de las transacciones en el Blockchain

El registro de las transacciones, igual que en Bitcoin, se realiza
combinándolas en bloques y superando una prueba conocida como Proof-of-Work.

Hoy en día se está migrando a una prueba basada en Proof-of-Stake en lugar del
PoW.

La consecuencia del minado es un cambio del estado del *singleton* global.

#### Multifirma

Viendo el formato de las transacciones, se entiende que no es posible firmar
las transacciones por más de una cuenta. Efectivamente, Ethereum no soporta la
multifirma. Para ella, es necesario crear un contrato que las soporte.


## Smart contracts

Los *smart contracts* son programas inmutables, deterministicos ejecutados en
la máquina virtual de Ethereum (EVM) que forma parte del *world computer*. El
nombre *smart contract* fue introducido por Nick Szabo en los años 90, pero el
concepto ha tomado otros significados desde entonces. La realidad es que los
*smart contracts* de Ethereum no son necesariamente un contrato: son programas
arbitrarios con las siguientes características:

- No pueden cambiarse una vez publicados: Inmutabilidad
- Su output es siempre el mismo para cualquiera que lo ejecute en su contexto:
  Determinismo
- Tienen un contexto de ejecución limitado formzado por la EVM. Sólo tienen
  acceso a:
    - Su estado
    - El contexto de la transacción que los invoca
    - Alguna información sobre bloques recientes
- Todas las EVM comparten estado, por lo que es indiferente dónde se ejecute el
  *smart contratct*: World computer

### Ciclo de vida del smart contract

1. Los smart contract se escriben, generalmente, en un lenguaje de alto nivel.
2. Se compilan a bytecode de la EVM
3. Se escriben en el Blockchain con la transacción de creación de un contrato,
   descrita anteriormente.
4. Se asigna una dirección al contrato, derivada de la cuenta que lo creó y el
   nonce de la transacción.

A partir de aquí, el creador pierde cualquier privilegio sobre el contrato: no
puede editarse, borrarse, etc. El autor no dispone de ninguna capacidad
especial sobre el contrato.

Los contratos sí que pueden destruirse, pero deben autodestruirse. La orden
`SELFDESTRUCT` destruye el contrato y devuelve gas, pero debe estar programado
como parte del propio contrato. El contrato no desaparece del Blockchain, pero
se invalida.

### Lenguajes de programación

Existen muchos lenguajes de programación diseñados para compilarse a bytecode
de la EVM.

La inmutabilidad de los contratos inteligentes y la necesidad de que estén bien
programados hace que sea interesante utilizar lenguajes de programación
funcionales. La realidad es que, a pesar de comenzar con un dialecto de lisp
como lenguaje principal, como Ethereum pretende llegar a todo el mundo creó un
lenguaje muy similar a JavaScript llamado Solidity para programar los smart
contracts. Posteriormente se creó Vyper, debido a un profundo estudio en los
smart contracts registrados que llegó a la conclusión de que muchos contratos
tenían errores graves (contratos suicidas, contratos avariciosos, etc).

Aunque Solidity sigue siendo el lenguaje de programación más común, Vyper es un
lenguaje diseñado para ser más estricto y limitar la posibilidad de cometer
errores. Existen además varios lenguajes más. Todos, por supuesto, deben
compilarse a bytecode de la EVM.

### Estándar para tokens: ERC-20

El ERC-20 (*Ethereum Request for Comments* número 20) propuso una forma
estándar de crear *tokens* en un smart contract.

Básicamente, el ERC-20 describe las reglas a cumplir por un contrato que
gestione una moneda, sistema de puntos, acciones o cualquier sistema similar.
Los contratos que cumplan con el estándar ERC-20 deben disponer de los
siguientes campos y funciones:

- Funciones opcionales:
    - `name`: devuelve el nombre legible del *token*.
    - `symbol`: devuelve el símbolo del *token*.
    - `decimal`: devuelve la cantidad de decimales disponibles para subdividir
      el token (18 máx.)
- Funciones Obligatorias:
    - `totalSupply`: devuelve la cantidad total de token exitentes.
    - `balanceOf`: recibe una dirección y devuelve la cantidad de tokens en esa
      dirección
    - `transfer`: recibe una dirección y una cantidad y transfiere la cantidad
      recibida a la dirección recibida desde la cuenta que realiza la
      transacción.
    - `transferFrom`: recibe un origen, un destino y una cantidad. Envía la
      cantidad del origen al destino. Se usa en conjunción con `approve`.
    - `approve`: recibe un destino y una cantidad. Autoriza a la cuenta de
      destino a realizar transacciones de menores de la cantidad desde la
      cuenta que envía la transacción.
    - `allowance`: recibe una dirección de dueño y una de gastador. Devuelve la
      cantidad restante que el gastador está autorizado a gastar del dueño.
- Eventos obligatorios:
    - `Transfer`: evento disparado al realizar una transferencia con éxito.
    - `Approval`: evento disparado al realizar una aprobación con éxito
      (`approve`).

> NOTA: Las carteras capaces de comunicarse con un contrato ERC-20 gestionan
> internamente las órdenes necesarias, pero es necesario saber que las
> transferencias de token a realizar no se hacen hacia la cuenta de la persona
> a quien quiere enviarse. Las transferencias se realizan al contrato, y la
> dirección a quien se quiere enviar la cantidad ha de enviarse como dato de la
> transferencia. Esto puede parecer extraño, pero el token sólo existe dentro
> del contrato y es él quien debe gestionarlo. Para la red Ethereum sólo
> existen los Ether, lo demás sólo son datos fluyendo por ella.

## Oráculos

Los oráculos son una forma sin-confianza para obtener datos externos aplicables
en los smart contracts.

- Cogen datos off-chain.
- Los combinan on-chain con una firma.
- Los dejan accesibles introduciéndolos en un smart contract.

Una vez los datos están en el contrato son accesibles de forma determinística
(todos los nodos de la red disponen de los mismos datos) mediante la función
`retrieve`.

Tres configuraciones principales:

1. Request-response
2. Publish-subscribe
3. Immediate-read

Además de para entregar datos, pueden utilizarse para realizar computación y
así reducir la carga computacional del smart contract, lo que reduce el gas
necesario para su ejecución.

Los oráculos también pueden ser elementos descentralizados en la red, existen
plataformas para ello, pero son ajenos al blockchain.

> NOTA: Los oráculos añaden funcionalidades necesarias para los smart contracts
> pero son peligrosos. Usar oráculos centralizados supone un punto único de
> fallo que compromete la aplicación completa. Es necesario ser muy cuidadoso
> con su uso.


## Decentralized Applications: DApps

Las DApps son aplicaciones (mayormente) descentralizadas. Están formadas por
diferentes apartados descentralizables:

- Backend
- Frontend
- Data-storage
- Sistema de comunicaciones
- Sistema de resolución de nombres

En comparación a aplicaciones no descentralizadas las DApps aportan:

- **Resiliencia**: Llevar la lógica de negocio a un smart contract hace que la
  aplicación esté operativa mientras el blockchain esté accesible.
- **Transparencia**: El DApp siempre puede consultarse porque está escrito en
  un Blockchain público y auditable.
- **Resistencia a censura**: La aplicación está disponible en cualquier nodo de
  la red, para eliminarla es necesario derribar la red completa.

### Estructura de un DApp

#### Backend

El *business logic* de la aplicación se guarda en un smart contract. Importante
tener en cuenta el límite del gas. Si el contrato es muy complejo puede ser muy
costoso de ejecutar.

#### Frontend

Normalmente los frontend de las DApps son aplicaciones Web que hacen uso de la
librería `web3.js` para su labor. Los DApps pueden ser cualquier otro tipo de
aplicación, pero no se dispone de un gran abanico de librerías avanzadas para
otros sistemas.

#### Data Storage

Debido al coste de los smart contracts, el storage suele llevarse fuera de
éstos.

- **Centralizado**: El data storage centralizado puede ser un simple servidor
  web sirviendo los ficheros necesarios.

- **Descentralizado**: Normalmente usando
  [Swarm](http://swarm-gateways.net/bzz:/theswarm.eth/),
  [IPFS](https://ipfs.io/) o alguna red distribuida similar.

> NOTA: Swarm es un storage content-addressed diseñado por la Ethereum
> foundation. IPFS es un storage content-addressed creado por la empresa
> Protocol Labs.


#### Messaging

Si la DApp necesita de un sistema de mensajería puede utilizarse uno
centralizado o llevarse a un sistema descentralizado como
[Whisper](https://github.com/ethereum/wiki/wiki/Whisper), creado al igual que
Swarm, por la Ethereum Foundation.


### Ethereum Name Service (ENS)

ENS es un sistema de resolución de nombres (como DNS), creado para la red de
Ethereum y sus aplicaciones. Está definido en: EIP-137, EIP-162 y EIP-181.

Sigue una filosofía *sandwitch* donde las capas superior e inferior son simples
y la capa intermedia es la más compleja.

En realidad, el propio ENS es una DApp diseñada para dar servicio a otras
DApps. Por tanto, es descentralizado.

Históricamente, el diseño partió de NameCoin, luego la funcionalidad se llevó a
un smart contract llamado "namereg" y finalmente se publicó la DApp ENS.

#### Capa inferior: Name Owners y Resolvers

ENS opera sobre "nodos" en lugar de nombres. La conversión nombre-nodo se
realiza usando Namehash.

La base es un smart contract sencillo (ERC-137) que permite a los dueños del
contrato añadir información sobre su "nodo" y "subnodos" (equivalente a dominio
y subdominio en DNS). La información incluye el *resolver* del nodo, el TTL,
hacer cambios de dueño, etc.

##### Namehash

Namehash es un algoritmo recursivo que convierte un nombre en un hash que lo
identifica. Se define de la siguiente manera:

```
namehash([]) = 0x0...0    (32 bytes nulos)
namehash([etiqueta, ...]) = keccak256( namehash(...) + keccak256(etiqueta) )
```

Lo que en python se define como:

``` python
def namehash(name):
  if name == '':
    return '\0' * 32
  else:
    label, _, remainder = name.partition('.')
    return keccak256(namehash(remainder) + keccak256(label))
```

Siendo la etiqueta (label) los identificadores de nodo y subnodo tal que
"ejemplo.ethereum.eth" está compuesto por las etiquetas "ejemplo", "ethereum" y
"eth".


##### Nómbres válidos

- Menos de 64 caracteres por etiqueta (label)
- Menos de 255 caracteres en el nombre completo
- Las etiquetas no pueden empezar ni terminar por dígitos o guiones

##### Dueños de los Top-Level Domain (TLD)

Se pretende descentralizar a los dueños de los TLD, pero actualmente el TLD se
gobierna con un contrato multifirma 4-a-7.

##### Resolver

El contrato ENS básico no añade metadatos a los nombres, eso deben hacerlo los
contratos *resolver*, un tipo de contrato que ha de crear el usuario para
aportar la funcionalidad.

El proceso de resolución consta de dos pasos:

1. El *registry* de ENS se llama con el nombre a resolver hasheado. Si existe,
   responde con su *resolver*
2. Se llama al *resolver* con la petición que corresponda y éste responde con
   la dirección final

Este proceso en dos etapas permite flexibilizar la aplicación y permitir al
resolver añadir variedad de funcionalidades.

#### Capa intermedia: Nodos .eth

Actualmente sólo se usa ".eth", pero se espera añadir otros TLDs en el futuro.

Los nodos ".eth" se reparten por subasta Vickrey.

##### Subastas Vickrey y el Blockchain

Las subastas Vickrey son un formato de subasta de puja sellada. Una vez creadas
todas las pujas, los participantes las revelan siendo el ganador el que aporte
una puja más alta. El ganador sólo debe pagar el importe pujado por el segundo.

Este formato de subastas tiene ciertas implicaciones en el contexto del
blockchain:

- Debe haber un sistema de bloqueo de fondos que asegure que el participante
  dispone del dinero necesario para pagar al momento de revelar las apuestas.
- Como en el blockchain es imposible guardar secretos es necesario realizar dos
  transacciones: la de puja y la de revelado.
- Es imposible revelar todas las pujas a la vez, los usuarios tienen que
  hacerlo ellos mismos. Además, la alternativa a revelar requiere de un castigo
  en modo de pérdida de fondos para que los usuarios se vean forzados a revelar
  su puja.

##### Proceso de la subasta

1. **Empieza la subasta**: Se envía a todos los nodos de la red (broadcast) la
   intención de registrar el nombre. El nombre no se envía en claro, se envía
   hasheado para que sólo las personas que estuvieran interesadas en él
   anteriormente sepan a cuál se refiere.
2. **Puja sellada**: Desde el momento del anuncio del comienzo de la subasta se
   dispone de 72 horas para pujar. La puja se realiza añadiendo un mensaje
   secreto (que contiene el hash del nombre por el que se puja, la cantidad y
   un salt) con una cantidad de moneda superior o igual a la cantidad por la
   que se puja. La moneda registrada sirve como aval y puede añadirse más e la
   cantidad que se quiere pujar realmente como despiste para el resto de
   participantes.
3. **Revelado de las pujas**: Desde el momento de cierre, cada participante
   dispone de 48 horas para revelar su puja realizando una transacción de
   revelación. Cada vez que un participante hace su revelación, el contrato
   calcula el ganador provisional y el segundo. Al acabar el tiempo el ganador
   provisional pasa a ser el ganador final.
4. **Limpiado**: El ganador recibe la diferencia de la cantidad añadida en su
   transacción y la puja del segundo clasificado. Si algún participante se
   olvidó de revelar puede hacerlo en esta etapa, pero sólo recuperará el 0,5%
   de la cantidad que añadió en la transacción.


#### Capa superior: Deeds

La capa superior guarda el dinero de la subasta durante la duración del
contrato. Para no guardar demasiado dinero en un único contrato, crea contratos
independientes para cada nodo. Estos contratos se llaman *deed contracts*.

Los deed contracts sólo pueden devolver el dinero a una persona (el dueño del
nodo o *deed owner*) y sólo pueden ser llamados por el contrato padre (el
*registrar contract*).


## Máquina virtual de Ethereum: EVM

La máquina virtual de Ethereum o EVM es una máquina virtual que, al igual que
la máquina virtual de Java (JVM) u otras, ejecuta *bytecode* compilado
específicamente para ella.

La labor de la EVM es gestionar la publicación y la ejecución de los smart
contracts. Puede entenderse como una máquina global descentralizada con
millones de objetos ejecutables, cada uno con su almacenamiento propio.

Es un entorno quasi-turing-complete ya que no es posible realizar bucles
infinitos debido a las limitaciones impuestas por el gas.

Su arquitectura está basada en un stack de palabras de 256 bits, donde se
guardan los elementos en memoria, aunque dispone de varias zonas
direccionables:

- El **código de programa** inmutable: el bytecode a ejecutar
- **Memoria volátil** inicializada a 0
- **Almacenamiento permanente** como parte del estado de Ethereum
  inicializado a 0
- Y algunas **variables globales**

La EVM es, como la ya mencionada JVM, es una forma agnóstica al sistema
operativo subyacente de ejecutar programas (muchos lenguajes de programación
usan este sistema). Como máquina de ejecución tiene las siguientes
características:

- Monohilo (estilo JavaScript)
- No tiene acceso al hardware
- Soporta operaciones
    - Lógicas
    - Aritméticas
    - Relacionadas con el Stack
    - De flujo de programa (JUMP...)
    - De sistema: crear cuentas, llamar a cuentas, etc.
    - De entrono: consultar gas price, etc.
    - De bloque: obtener la transacción coinbase del bloque, el timestamp, etc.

> NOTA: Recursos adicionales para estudiar la EVM:
>
> [https://github.com/ethereum/wiki/wiki/Ethereum-Virtual-Machine-(EVM)-Awesome-List](https://github.com/ethereum/wiki/wiki/Ethereum-Virtual-Machine-(EVM)-Awesome-List)

### Estado Ethereum

La EVM actualiza el estado del singleton global que es Ethereum pero ¿qué es el
estado?

Desde un punto de vista de alto nivel el estado de Ethereum es una asociación
entre direcciones y cuentas (*world state*).

Desde un punto de vista de bajo nivel el estado de Ethereum es una asociación
entre direcciones y un conjunto de datos:

- Balance: Cantidad de ethers controlados por esta cuenta
- Nonce:
    - Si la cuenta es un EOA: Número de transacciones correctas realizadas
    - Si la cuenta es una Contract account: Número de contratos creados
- Storage: espacio para almacenar datos (no existe en las EOA)
- Program Code: código del smart contract (no existe en las EOA)

### Proceso de ejecución

1. Una transacción desde un EOA a un smart contract dispara su ejecución. Los
   contratos pueden dispararse desde otro contrato, pero inicialmente alguien
   tiene que haber disparado el primero desde un EOA y haber indicado el límite
   de gas.
2. Se carga la EVM con el gas de la transacción.
3. Se ejecuta el contrato. Cada orden de la EVM consume el gas asociado con esa
   orden.
4. La ejecución termina. Dos modos alternativos de terminar:
    1. La ejecución termina de forma exitosa y el gas sobrante se devuelve a
       quien disparó el contrato
    2. Durante la ejecución del contrato el gas se agota antes de terminar por
       lo que el contrato se termina con un error y el gas se consume.

### Publicación del contrato

Es necesario tener en consideración que cuando el contrato se publica,
realizando la transacción a la dirección `0x0...0` el código que se añade es
diferente al que se ejecutará. El código de iniciación o *initiation code* es
más extenso que el *runtime bytecode* porque dispone de código necesario para
la inicialización y creación.

En realidad, el *runtime bytecode* es parte del *initiation code* pero éste
segundo tiene un poco más.

### Gas

Cada orden consume una cantidad de gas establecida por el propio diseño de
Ethereum. Por ejemplo:

- Sumar dos números cuesta 3 gas.
- Calcular Keccak-256 cuesta 30 gas y 6 adicionales por cada 256 bits de datos
  de input.
- Enviar una transacción cuesta 21000 de gas.

Estos costes tratan de ser proporcionales al coste de computación de cada orden
para ser justos.

> NOTA: El coste de gas por orden ha sido cuidadosamente seleccionado. Un
> atacante encontró una incoherencia en los costes y fue capaz de ejecutar
> contratos con órdenes baratas con mucha carga computacional. Gracias a eso
> fue capaz de bloquear la red de Ethereum. Los costes tuvieron que reajustarse
> tras el incidente.

El gas tiene dos roles principales:

- Premiar a los mineros
- Defenderse frente a ataques de negación de servicio (Denial-of-Service attack
  DoS)

#### Conteo del gas

$$
fee = gas_{cost} \cdot gas_{price}
$$
$$
refund = (gas_{limit} - gas_{cost}) \cdot gas_{price}
$$

Como se ha dicho anteriormente si el gas se gasta durante la ejecución, no se
devuelve nada.

El hecho de que el gas sea una moneda independiente que se intercambia por
Ethers es un truco para poder mantener fijos los costes por operación mientras
que el coste total en ether puede fluctuar.

Como en bitcoin, los mineros eligen las transacciones que más les interesan, en
este caso las que tienen un `gasPrice` más alto normalmente.

El `gasLimit` sólo indica la cantidad máxima de gas a pagar, no afecta a los
mineros porque no se puede predecir cuánto durará la ejecución del contrato.

##### Coste de gas negativo

Hay varias operaciones que tienen un coste de gas negativo para incentivar la
limpieza del blockchain:

- `SELFDESTRUCT` devuelve 24000 de gas: Operación de autodestrucción del
  contrato.
- `SSTORE[x]=0` devuelve 15000 de gas: Implica vaciar un campo del
  almacenamiento del smart contract.

El total del gas que se devuelve es como máxima la mitad del gasto total de gas
para evitar que se explote este comportamiento con fines maliciosos.

#### Block gas limit

El block gas limit indica la cantidad total de gas consumible por todas las
transacciones del bloque. Siendo así, limita la cantidad de transacciones que
se pueden incluir en un bloque.

El block gas limit se decide de forma colaborativa, cada minero puede votar en
subirlo o bajarlo por un factor de $1/1024$.

Este mecanismo de votos se combina con una estrategia donde el mínimo de gas
limit parte de 4.7 millones y se reajusta al 150% de la media del gasto por
bloque cada 1024 bloques de forma similar al ajuste de la dificultad de
Bitcoin.

## Consenso

Lo aprendido en Bitcoin aplica al consenso de Ethereum pero no es necesario
conocer este mecanismo para desarrollar smart contracts.

### Proof-of-Work

Ethereum usa un algoritmo de Proof-of-Work radicalmente distinto al de Bitcoin
aunque su objetivo es el mismo.

Ethereum usa el PoW  [Ethash](https://github.com/ethereum/wiki/wiki/Ethash),
una evolución de la función Dagger-Hashimoto. Ethash depende del análisis y
generación de un dataset de gran tamaño llamado DAG (Direct Acyclic Graph).

El DAG comienza con 1GB de tamaño y aumenta cada 30.000 bloques (unas 125
horas).

Ethash es una función *memory-hard*, como CryptoNight, que se basa en acceder
en repetidas ocasiones a un dataset muy grande. Esto hace que sea una función
resistente a ASICs.

### Proof-of-Stake

Históricamente se propone el Proof-of-Stake antes que el Proof-of-Work para las
criptomonedas, una de las innovaciones traidas por Bitcoin es la aplicación de
un Proof-of-Work.

En Ethereum desde el principio quiso utilizarse un algoritmo de Proof-of-Stake,
pero comenzaron con el Proof-of-Work. Para forzarse a cambiar al
Proof-of-Stake, añadieron una bomba de dificultad (*difficulty-bomb*) en el
proceso Proof-of-Work: El aumento del tamaño del DAG.

La propuesta del PoS se ha ido prorrogando y han tenido que reajustar la bomba
de dificultad en varias ocasiones pero la intención final es migrar a un PoS
llamado Casper.

El proceso PoS funciona de la siguiente manera:

1. Algunos nodos se registran como *validator* enviando una transacción
   especial con unos fondos.
2. Los *validator* se turnan para proponer y votar por los bloques. La
   probabilidad de que a un validator le toque proponer un bloque es
   proporcional a los fondos registrados en el paso 1. Igualmente, su peso en
   la votación es proporcional a los fondos.
3. Si el bloque propuesto se acepta, el *validator* recibe un incentivo. Si el
   bloque no se acepta, el validator que lo propuso se arriesga a perder los
   fondos.

### Diferencia entre PoW y PoS

La pérdida de dinero asociada a minar un bloque rechazado en el Proof-of-Stake
es intrínseca al blockchain, la propia implementación es la que bloquea tus
fondos.

El Proof-of-Work, por contra, tiene un gasto estrínseco: Provoca al operador
del minero gastar energía.

### Principios del consenso

Para crear un algoritmo de consenso es necesario responder a unas preguntas,
con ellas se puede conseguir el fundamento sobre el que desarrollar:

- ¿Quién puede cambiar el pasado?¿Cómo? (concepto de *inmutabilidad*)
- ¿Quién puede cambiar el futuro?¿Cómo? (concepto de *finalidad*)
- ¿Cuál es el coste de los cambios?
- ¿Cómo de descentralizado es el poder de decisión de los cambios?
- ¿Cómo se sabrá que algo ha cambiado?¿Quién lo sabrá?


### Minado en Ethereum

Independientemente del algoritmo PoW o PoS que se elija, el minado en Ethereum
tiene unas características adicionales que lo diferencian del proceso de minado
y validación de Bitcoin.

El minado de un nuevo bloque en Ethereum se realiza, de media, cada 15 segundos
(frente a los 10 minutos de Bitcoin).

El premio actual por minado de bloque se divide en estos factores:

- Incentivo por bloque: 5 Ether
- El gas asociado al bloque (como se vió anteriormente)
- Un premio adicional por cada *uncle block* correspondiente a 1/32 partes del
  incentivo (pueden incluirse hasta 2 *uncle blocks*)

Los *uncle block* u *ommer block* son un concepto que provienen del protocolo
GHOST (*Greedy Heaviest Observed Subtree*) y sirve para denominar a los bloques
correctos que han sido minados a la vez que otros y no forman parte de la
cadena más larga (existen algunas condiciones adicionales). En Bitcoin la
probabilidad de la aparición de uno de estos bloques es muy baja debido a la
frecuencia de minado a la dificultad. En Ethereum, sin embargo, como es
necesario publicar bloques en un ratio mayor, se aplicó el protocolo GHOST con
la intención de tener bajo control las dispersiones producidas.

Los mineros que minan un *uncle block* reciben 30/32 partes del premio por
minado.

El protocolo GHOST sirve para compensar los problemas posibles de los
blockchains con una frecuencia de bloque elevada.

### Validación en Ethereum

En Ethereum es necesario incluir información adicional en los bloques que
define el *estado* del que se ha hablado durante la descripción de Ethereum. En
lugar de un sólo Merkle-Tree como en Bitcoin, en Ethereum se guardan los root
hash de varios Merkle-Patricia-Tries (del *world state*, de los receptores de
las transacciones, de las transacciones y del *storage*). Todos ellos validan
por los nodos al recibir los nuevos bloques. Cada nodo compara los root hash de
todos esos árboles con el estado que ellos mismos computan.
