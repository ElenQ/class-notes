# Bitcoin

## Por qué Bitcoin

El Blockchain fue diseñado expresamente para solventar el problema que Bitcoin
plantea, pero no es más que una pequeña parte de lo necesario para asegurar una
transferencia de valor adecuada en un ecosistema descentralizado.

Para entender el problema que se resuelve con el Blockchain es necesario
entender Bitcoin, pues Bitcoin es la razón de su existencia y es en Bitcoin
donde se puede ver cuáles son las tareas que el Blockchain desarrolla y cuales
no.

Más allá de eso, todas las monedas electrónicas modernas y aplicaciones
derivadas parten de algún modo de Bitcoin. Además, Bitcoin es maduro y está
bien documentado por lo que es mucho más sencillo entender los conceptos de
éste que de otras tecnologías similares.

Bitcoin servirá por tanto de fundamento principal para explicar por qué es
necesario un Blockchain, para saber cuales son las tareas que cumple y cuales
son las que no cumple un Blockchain y para establecer las bases necesarias de
criptografía y diseño de aplicaciones distribuidas para entender cualquier otra
aplicación relacionada.

## Qué es Bitcoin

- Un conjunto de tecnologías que consiste en:
    - Red p2p descentralizada: *Protocolo Bitcoin*
    - Resumen de cuentas público: *Blockchain*
    - Reglas de validación: *Consenso*
    - Mecanismo para llegar al consenso: *Prueba de trabajo (Proof-of-Work)*
- Este conjunto de tecnologías manipula unidades monetarias llamadas *bitcoin*
  que sirven para **transmitir valor**.

## Funcionamiento general

[Paper original](https://bitcoin.org/bitcoin.pdf).

Algunas referencias sencillas:

- [Charla: Bitcoin para anarquistas](https://ekaitz-zarraga.gitlab.io/talks/Bitcoin_For_Anarchists/)
- [Artículo: De Bizancio y la realidad de lo cotidiano](https://pfctelepathy.wordpress.com/2018/09/11/de-bizancio-y-la-virtualidad-de-lo-cotidiano/)

## Claves y direcciones

- Criptografía asimétrica: Parejas pública-privada
- La criptografía se usa como *firma electrónica*.
- Las Carteras (*wallet*) controlan las claves y direcciones.
- La clave pública se obtiene de la privada pero se suelen guardar ambas.
- La clave privada se obtiene aleatoriamente (256 bit)

### Generar la clave privada

Aunque se hace aleatoriamente, es necesario utilizar una función de alta
entropía para esto.

> CONCEPTO: CSPRNG *Cryptographically Secure Pseudorandom Number
> Generator*

Las claves privadas tienen 256 bit:

$$
k \in [1, 1.158 \cdot 10^{77}]
$$

#### Ejemplos

**Con Bitcoin Explorer**

``` bash
$ bx seed | bx ec-new | bx ec-to-wif
    PRIV_KEY
```

**Con Bitcoin Core Client**

``` bash
$ bitcoin-cli getnewaddress
    PUB_KEY
$ bitcoin-cli dumpprivkey PUB_KEY
    PRIV_KEY
```

> NOTA: El formato WIF (*wallet import format*) es un formato basado en
> Base58-Check.

> NOTA: Bitcoin Core Client no entrega la clave privada directamente, hay que
> pedirle de forma explícita que la exponga.

### Clave pública

$$
K = k \cdot G
$$

Donde:  
$K$ = Clave pública  
$k$ = Clave privada  
$G$ = Generator point de la curva elíptica  

#### Criptografía de curva elíptica

Bitcoin usa la curva `secp256k1`, que se define así:

$$
y^2 = (x^3 + 7) \bmod p
$$

> NOTA: Modulo p significa que se aplica sobre un cuerpo finito donde p es un
> número primo de valor:
> $$ p = 2^{256} - 2^{32} - 2^9 - 2^8 - 2^7 - 2^6 - 2^4 -1 $$

![Proceso de suma en la curva elíptica](img/elliptic-curve.jpeg)

Características de la curva elíptica:

- Suma: $P_3 = P_1 + P_2 \Rightarrow P_3$ *también está en la curva.*  
  La suma se obtiene conectando los puntos a sumar, la recta que los conecte
  (siempre) tocará la curva. El resultado de la suma es el reflejo vertical de
  ese punto de corte (misma `x` voltear en `y`: $P_3' = (x,y) \Rightarrow P_3 =
  (x,-y)$).  
  Para sumar $P_1 + P_1$ hay que obtener la tangente y prolongarla hasta que
  toque.

- Punto en el infinito: $P_1 + P_2 = P_2$ *si $P_1$ es el punto en el
  infinito.*  
  Se conoce como punto en el infinito al resultado de sumar dos puntos en la
  misma vertical (misma `x` y diferente `y`). *Se comporta como un cero*.

- Propiedad asociativa: $(A + B) + C = A + (B + C)$  
  Sabiendo esto se puede definir la multiplicación como: $A + A + ... + A = k
  \cdot A$

> NOTA: se suele conocer `k` como *exponente*. Puede llevar a equívoco.

#### Generar la clave pública

$$ K = k \cdot G $$

Básicamente suma `G` a sí mismo `k` veces.

El punto `G` es el mismo para todo Bitcoin.

> NOTA: Suele usarse OpenSSL como librería para esto.


### Direcciones

Pueden apuntar a una pareja de claves pública-privada o algo más complejo como
un script.

Así se obtiene la dirección de una pareja de claves: desde la clave privada.
Ambas funciones son funciones de hashing. El resultado tiene 160 bits.

```
A = RIPEMD160( SHA256( K ) )
```

Normalmente se representan en Base58-Check.

#### Base58-Check

Es un formato para codificar datos binarios en modo texto, basado en BASE64
pero eliminando los caracteres que pueden llevar a equívoco.

Se utiliza para claves privadas, públicas, direcciones y scripts.

Alfabeto:

```
123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz
```

El check es un simple checksum que sirve para comprobar los posibles errores,
como la letra del DNI.

El proceso de codificación Base58-Check se realiza de la siguiente forma:

1. Al payload (datos a codificar) se le añade un prefijo para marcar la
   versión, que indica el tipo de dirección de Bitcoin que se creará.
2. Se *hashea* con la función SHA256, y su resultado vuelve a *hashearse* con
   la misma función.
3. Los primeros 4 bytes del *hash* se añaden como sufijo al payload. Actúan de
   *checksum*.
4. El conjunto de Versión-Payload-Checksum se convierte a Base58 usando el
   diccionario anterior.

> NOTA: En este proceso, el número de versión introducido pasa a Base58 al
> final.  Algunos tipos de versión explotan este comportamiento para escribir
> cosas como `xpub` en el resultado, para lo que introducen un número de
> versión como `0x0488B21E` como es el caso de las claves públicas extendidas
> BIP32.

![Proceso de codificación Base58-Check](img/base58-check.jpeg)

[Más información](https://en.wikipedia.org/wiki/Base58)

#### Formatos de claves

Las claves tienen diferentes formatos (binario, hex, Base58...) y Base58-Check
sirve para representar claves tanto privadas como públicas. Es importante ser
consciente de los diferentes formatos y ser capaz de convertir de uno a otro
sin problemas.

##### Compresión de claves públicas

Las claves públicas están formadas por una pareja de valores que indican las
coordenadas `x` e `y` del punto que representan en la curva elíptica.
Conociendo la definición de la curva elíptica se puede deducir la `y` si se
sabe la `x`. Con el objetivo de reducir la cantidad de datos por transacción,
se creó un formato de clave pública que omite el valor `y`.

Existe un problema con la compresión: En la fórmula, `y` aparece elevada al
cuadrado, por lo que es necesario indicar si la `y` era positiva o negativa en
el proceso. Para solventar este problema se le añade diferente prefijo a la
representación de la clave en función de si `y` es par, que indicaría que es
positiva, (`02`) o impar, que indicaría que es negativa, (`03`).

Las claves públicas no comprimidas tienen prefijo `04` y se representan como
`04xy` mientras que las comprimidas se representan como `02x` o `03x`.



## Claves y direcciones avanzadas

- **Claves privadas encriptadas BIP-38**: BIP-38 es un estándar para encriptar
  claves mediante una contraseña.

- **Pay-to-Script Hash (P2SH) y multisig**:
    - Las direcciones normales empiezan por `1`, las P2SH por `3`. *Se les
      añade el prefijo 5 para que el resultado del Base58-Check tenga el
      prefijo 3*.
    - Se introdujeron en el BIP-16.
    - Las P2SH se crean desde un script que define cómo se gastan los fondos en
      lugar de una clave pública. El proceso es igual:  
      `script-hash = RIPEMD160( SHA256( script ) )`
    - El *multisig* es un tipo de P2SH en el que el script implica el uso de
      muchas firmas para liberar los fondos.

- **Vanity addresses**: A fuerza bruta se pueden "seleccionar" los primeros
  caracteres de una dirección. *Igual que en los hidden-services de Tor*.
  Las *vanity addresses* sirven para dar autenticidad pero si alguien se
  esforzase en crear una parecida podría engañar a los usuarios.

## Carteras

Dos tipos principales:

- **Non deterministic**: Crean las claves aleatoriamente
- **Deterministic**: Usan una semilla (*seed*) para crear las claves
  (estándares BIP-32 y BIP-44). Facilitan las migraciones.

Un conjunto de BIPs definen las buenas prácticas para las carteras (en
general):

- Nemónicos: BIP-39
- HD wallets: BIP-32
- Multipurpose HD: BIP-43
- Multimoneda y multicuenta: BIP-44

### Nemónicos: BIP-39

Los nemónicos son una secuencia de 12 a 24 palabras aleatorias. Es suficiente
para para recuperar la cartera completa. Existen dos modos principales para
hacerlo, aunque son similares:

- El BIP-39 fue propuesto por Trezor.
- Otro estándar usado en Electrum Wallet.

#### Proceso

1. Generar entropía 128-256 bits
2. Obtener checksum de los $l/32$ primeros bits de su SHA-256
3. Añadir checksum al final
4. Dividir la secuencia en secciones de 11 bits
5. Mapear cada secuencia a una palabra del diccionario de 2048 palabras
6. El nemónico será esa lista de palabras
7. PBKDF2 Key-Stretching usa el nemónico como input
8. El segundo parámetro es un *salt*:  
   `salt = "mnemonic" + user_defined_passphrase`
9. PBKDF2 aplica 2040 rondas de HMAC-SHA512 que producen 512 bits de output
10. Ese output es la semilla (*seed*) real.

> NOTA: Todos los passphrase llevan a una semilla válida, por lo que pueden
> usarse varios sobre el mismo nemónico para ocultar fondos u otras funciones.

> NOTA: PBKDF2 es una función de derivación de claves para reducir la
> vulnerabilidad de claves a un ataque fuerza bruta. Añade coste computacional
> a una clave (lo que se conoce como *key stretching*) para hacer inviable un
> ataque por fuerza bruta. [Más información](https://en.wikipedia.org/wiki/PBKDF2)

Algunas librerías interesantes:

- [python-mnemonic](https://github.com/trezor/python-mnemonic)
- [bitcoinjs/bip39](https://github.com/bitcoinjs/bip39)
- [libbitcoin/mnemonic](https://github.com/libbitcoin/libbitcoin/blob/master/src/wallet/mnemonic.cpp)


### Carteras HD (BIP-32 / BIP-44)

Las carteras HD son el tipo de cartera más avanzado dentro de las carteras
determinísticas. HD significa *Hierarchical Deterministic*, lo que implica que
las direcciones nacen de una semilla, pero en forma de árbol.

Parten de una semilla que puede ser de 128, 256 o 512 bits y probablemente
venga de un nemónico como los explicados en el apartado anterior.

#### Creación de la cartera desde la semilla

La semilla se introduce en HMAC-SHA512 y se divide en dos mitades de 256 bits.

De la mitad izquierda se obtiene la clave privada maestra `m`, de la que se
puede obtener la clave pública maestra `M` mediante la multiplicación de curva
elíptica aprendida antes:

$$ M = m \cdot G $$

La mitad derecha se utilizará como *Chain code* `c` para añadir entropía al
proceso.

![Proceso de creación de la cartera desde la semilla](img/master-derivation.jpeg)

#### Claves extendidas

Las claves extendidas son un formato de representación de claves que incluye el
Chain code para dar acceso a la rama completa.

Pueden crearse con la clave privada o pública.

Se codifican con Base58-Check introduciendo los prefijos adecuados que
resultan en `xpub` y `xprv` y tienen esta forma:

```
xpub661MyMwAqRbcFtXgS5sYJABqqG9YLmC4Q1Rdap9gSE8NqtwybGhePY2gZ29ESFjqJoCu1Rupje  
8YtGqsefD265TMg7usUDFdp6W1EGMcet8

xprv9s21ZrQH143K3QTDL4LXw2F7HEK3wJUD2nW2nRk4stbPy6cq3jPPqjiChkVvvNKmPGJxWUtg6L  
nF5kejMRNNU3TGtRBeJgk33yuGBxrMPHi
```

#### Derivación de claves

Se necesitan:

- Clave pública o clave privada padre (con la privada se puede deducir la
  pública) (`K` o `k`)
- Una semilla llamada *chain code* (`c`)
- Un índice de 32 bit (`i`)

La labor del índice (*index*) es saber qué hijo se está obteniendo con el
proceso para poder nombrarlos. Cada padre, por tanto, puede tener 2³² hijos.

El *chain code* añade entropía para complicar la deducción de los hijos, de
esta forma **es imposible saber el hijo sólo a partir del padre y del índice**.

El proceso de derivación crea claves con las que se puede conseguir direcciones
y firmar. **Son como cualquier otra clave**.

El proceso usa la función de una sola dirección HMAC-SHA512, asegurando que no
puede obtenerse el padre desde el hijo.

> NOTA: para más información del proceso de derivación, consultar el BIP-32
> [https://github.com/bitcoin/bips/blob/master/bip-0032.mediawiki](https://github.com/bitcoin/bips/blob/master/bip-0032.mediawiki)

El proceso de derivación de claves (*CKD, child key derivation*) se basa en
varios puntos:

##### Private parent key → private child key

$$
CKD_{priv}((k_{par}, c_{par}), i) \Rightarrow (k_i, c_i)
$$

Comprueba si el índice es *hardened* (leer más abajo) y en función de ello
realiza la creación de forma diferente:

- Si el índice **es hardened** el proceso inserta la clave privada padre
  directamente (con un padding inicial).

- Si el índice **no es hardened** el proceso extrae la clave pública de la
  clave privada padre y la utiliza en el proceso.

##### Public parent key → public child key

$$
CKD_{pub}((K_{par}, c_{par}), i) \Rightarrow (K_i, c_i)
$$

Este procedimiento **sólo está definido para hijos *non-hardened***.


##### Private parent key → public child key

Definiendo: $N((k, c)) \Rightarrow (K, c)$ como el proceso para obtener una clave pública
extendida desde una clave privada extendida, donde:

- $K$ se obtiene mediante la multiplicación de curva elíptica de $k$
- Y $c$ es el mismo para ambos

Existen dos formas equivalentes de obtener la clave pública hija a partir de la
clave privada padre:

- $N(CKD_{priv}((k_{par}, c_{par}), i))$ válida para cualquier caso
- $CKD_{pub}(N(k_{par}, c_{par}), i)$ válida sólo para índices *non-hardened*

De este modo, usando cualquier clave non-hardened, es posible obtener claves
públicas a través de una clave pública padre sin exponer la clave privada en
ningún momento y el resultado es equivalente al obtenido mediante la derivación
de la clave privada.

##### Public parent key → private child key

Este proceso **no es posible**.


#### Hardened keys

Las *hardened keys* son un tipo de claves *reforzadas* que se identifican con
los índices mayores que 2³¹. Estos índices se suelen indicar como $i_H$ o $i'$
para que no sea necesario utilizar cifras tan altas y representar que son
*hardened*. Para obtener el índice real:

$$
i = 2^{31} + i_H
$$

Las claves *hardened* son importantes para aportar seguridad. Se utilizan como
un *firewall*. Aunque no es evidente, conocer una clave pública extendida y
cualquier clave privada *non-hardened* derivada de ésta es equivalente a
conocer la clave privada extendida. Esto daría acceso a todo el árbol.

Las claves *hardened* son menos útiles que una clave normal, pero rompen ese
comportamiento para poder aislar ramas y evitan que el árbol completo se
comprometa tan fácilmente.

#### Identificación y aplicación de las claves

Sabiendo que las claves se representan a modo de árbol y que los índices
sirven para numerarlas, se define una identificación en modo árbol para las
claves muy similar al *path* en un sistema operativo de la familia UNIX.

Los paths comienzan con `M` si la clave ha sido derivada de la clave pública
maestra o `m` si han sido derivadas de la clave privada maestra. Después se
van añadiendo índices (con ' si es un índice hardened). Ejemplo:

- `M/0'/10`: es el hijo número 10 (el décimo primero porque empiezan por 0)
  del primer hijo *hardened* de la clave pública maestra.

- `m/1'/1`: es el segundo hijo del segundo hijo *hardened* de la clave privada
  maestra.

El estándar BIP-44 define una estructura de hijos que aporta lógica al árbol de
la siguiente forma:

```
m / purpose' / coin_type' / account' / change / address_index
```

Cada uno de los niveles tiene un significado asociado. [Pueden leerse en el
estándar BIP-44](https://github.com/bitcoin/bips/blob/master/bip-0044.mediawiki#Abstract)


#### Aplicaciones

- Compartir la cartera completa: `m`
- Auditorías: `N(m/*)`
- Balances por oficina: `m/i'`
- Transacciones recurrentes negocio-a-negocio: `N(m/i'/0)`
- Receptor de dinero inseguro: `N(m/i'/0)`



## Transacciones

Las transacciones son estructuras de datos que indican una transferencia de
valor dentro del ecosistema de Bitcoin.

> NOTA: para observar las transacciones pueden usarse los comandos
> `getrawtrasaction` y `decoderawtransaction` del Bitcoin Core.


### Inputs y Outputs

La parte fundamental de una transacción (*TX*) es el *output*. Los *output* son
bloques indivisibles de bitcoin que se guardan en el Blockchain y se consideran
válidos por toda la red. Los nodos de bitcoin analizan todos los *output* que
no han sido gastados y están disponibles (*unspent transaction outputs* o
*UTXO*).

Cuando se dice que una cartera recibe dinero lo que ocurre es que la cartera
detecta UTXO gastable usando una de las claves de las que la cartera dispone.
El *balance* de cada cartera es la suma de los UTXO que la cartera encuentra.

Los outputs son indivisibles y se representan con un número entero de
`satoshis`:

$$
1\ satoshi = 10^{-8}\ Bitcoin
$$

Las transacciones consumen UTXO de transacciones anteriores y crean una nueva
con un nuevo UTXO consumible por una futura transacción. Así es como el valor
cambia de manos en bitcoin.

> EXCEPCIÓN: Las transacciones *coinbase* no gastan UTXO anterior porque crean
> nuevos bitcoin como premio para los mineros.

El UTXO de una transacción es *indivisible* y *discreto*, esto implica que al
hacer una transacción ésta debe gastar todo el UTXO de la anterior. Para poder
cambiar la cantidad que se envía puede devolverse parte del UTXO anterior a la
misma dirección. Además, una transacción puede gastar varios UTXO y
combinarlos si necesita más *input* del que dispone en una sola dirección.

#### Outputs

Cada uno de los *outputs* de una transacción tiene dos campos:

- Los `satoshis`
- Un puzzle mágico que los libera

Se envían a la red serializados:

- Little Endian
- También con un campo de longitud

Campos de output de una transacción codificados como JSON para facilitar su
lectura. Bitcoin Core codifica el campo `value` en bitcoin, pero internamente
se enviaría en `satoshi`:

``` json
  "vout": [
    {
      "value": 0.01500000,
      "scriptPubKey": "OP_DUP OP_HASH160 ab68025513c3dbd2f7b92a94e0581f5d50f654e7
        OP_EQUALVERIFY OP_CHECKSIG"
    },
    {
      "value": 0.08450000,
      "scriptPubKey": "OP_DUP OP_HASH160 7f9b1a7fb68d60c536c2fd8aeaa53a8f3cc025a8
        OP_EQUALVERIFY OP_CHECKSIG",
    }
  ]
```

#### Inputs

Cada input de la transacción tiene los siguientes valores:

- El ID de la transacción (o transacciones) cuyo UTXO quiere gastarse. `txid`
- El índice que dice cuál de los UTXO de la transacción anterior quiere
  gastarse. *Comienzan por 0*. `vout`
- El desbloqueo del `script`: la solución al puzzle mágico. `scriptSig`
- Un *sequence number*. `sequence`

> NOTA: Esto significa que hay que recuperar la transacción anterior para
> entender la transacción actual en su totalidad.

También aquí los datos se envían serializados de una forma similar al *output*.

Ejemplo de campos input de una transacción codificados en JSON:

``` json
"vin": [
  {
    "txid": "7957a35fe64f80d234d76d83a2a8f1a0d8149a41d81de548f0a65a8a999f6f18",
    "vout": 0,
    "scriptSig" : "3045022100884d142d86652a3f47ba4746ec719bbfbd040a570b1deccbb6
        498c75c4ae24cb02204b9f039ff08df09cbe9f6addac960298cad530a863ea8f53982c0
        9db8f6e3813[ALL] 0484ecc0d46f1918b30928fa0e4ed99f16a0fb4fde0735e7ade841
        6ab9fe423cc5412336376789d172787ec3457eee41c04f4938de5cc17b4a10fa336a8d7
        52adf",
    "sequence": 4294967295
  }
]
```

#### Tasas

Las tasas sirven de incentivo para que los mineros trabajen.

- Las transacciones que pagan menos tasas de `minrelaytxfee` se consideran
  gratuitas y sólo se procesan si hay sitio en el `mempool`[^1]
- Están relacionadas con el tamaño de la transacción. Más tamaño más tasas.
- Las regula el mercado.
- Al implementar una cartera es mejor predecir las tasas necesarias para no
  dejar transacciones muertas.

Las tasas se añaden a la transacción por descarte:

$$
Fee = Sum(v_{in}) - Sum(v_{out})
$$

> WARNING: Es importante incluirse a uno mismo en la salida para recuperar las
> vueltas, si no se hace el dinero sobrante será enviado al minero como tasas.


[^1]: `minrelaytxfee` es una variable de configuración de Bitcoin Core, cada
nodo puede cambiarla y configurarla. El `mempool` es la memoria que el nodo de
Bitcoin utiliza para guardar transacciones pendientes de procesar por un
minero.

### Scripts

Los scripts son el puzzle mágico que hay que resolver para desbloquear los
fondos. SOn similares al lenguaje de programación Forth pero no son *turing
complete* **por seguridad**. No es posible hacer bucles.

> NOTA: Las transacciones son evaluadas por todos los nodos, si se permitiera a
> los scripts realizar cualquier acción se estaría permitiendo ejecutar código
> de forma arbitraria en cualquier equipo de la red de Bitcoin.

Los scritps son *stateless*. No guardan ningún estado, funcionan con *funciones
puras* y no tienen ningún *side-effect*.

#### Forma de los scripts

Existen dos tipos de scripts:

1. **Locking script**: sirve para bloquear los fondos. Tiene diferentes nombres
   en función de la aplicación:
    - `scriptPubKey`: Nombre histórico. Inicialmente los scripts de bloqueo
      solían tener la clave pública, por eso lleva ese nombre.
    - *witness script*: Con *Segregated Witness* (*SegWit*) el script gana otro
      significado. Se verá más adelante.
2. **Unlocking script**: sirve para desbloquear los fondos. Este script
   resuelve el puzzle propuesto por el *locking script*. Igual que el anterior,
   tiene varios nombres, pero se refieren siempre a lo mismo:
    - `scriptSig`
    - *witness*

Los *locking script* se añaden en el *output* de las transacciones para
bloquear el UTXO de éstas.

Los *unlocking script* se añaden en el *input* de las transacciones para
liberar los UTXO bloqueados por la transacción anterior.

##### Stack

*Script* es un lenguaje basado en un *stack* o cola *LIFO* (*Last In First
Out*). Cuando se ejecuta una orden toma sus argumentos de entrada del *stack* y
vuelca su resultado al *stack*. Para enviar argumentos de entrada a las
órdenes, hay que volcar valores al *stack* en primer lugar.

Ejemplo:

```
1 4 OP_ADD 5 OP_EQUAL
```

El ejemplo funciona de la siguiente manera:

1. Introduce el valor 1 en el stack.  
   Contenido del stack: `[1]`
2. Introduce el valor 2 en el stack.  
   Contenido del stack: `[1, 4]`
3. `OP_ADD` consume los dos últimos valores del stack, los suma (5) y añade el
   resultado al stack.  
   Contenido del stack: `[5]`
4. Introduce el valor 5 en el stack.  
   Contenido del stack: `[5, 5]`.
5. `OP_EQUAL` consume los dos últimos valores del stack y los compara, volcando
   el resultado (`TRUE` o `FALSE`) al stack.  
   Contenido del stack: `[TRUE]`

##### Validación

La validación de los scripts se realiza combinando ambos *locking* y
*unlocking*. El script de *unlocking* se ejecuta en primer lugar y su stack se
introduce como stack inicial para el *locking* script.

> NOTA: Inicialmente los scripts se combinaban y ejecutaban seguidos, un fallo
> de seguridad hizo que se ejecutaran separados para asegurar que el *unlocking
> script* no corrompía el stack del siguiente. Actualmente se ejecutan por
> separado para poder comprobar que no se han corrompido.

Las transacciones se consideran válidas si se cumple cualquiera de estas
condiciones:

- El resultado en la parte superior del stack al final de la
  ejecución es:
    - `TRUE`
    - Cualquier valor diferente de `0`
- El stack está vacío al final de la ejecución.

Por contra, la transacción se considera inválida si:

- El valor en la parte superior del stack es `FALSE`
- Si la ejecución del script ha sido interrumpida por algún comando como:
  `OP_VERIFY`, `OP_RETURN` o por un condicional como `OP_ENDIF`.


Siguiendo el ejemplo anterior, si el *locking script* fuera:

```
4 OP_ADD 5 OP_EQUAL
```

Y el *unlocking script* fuera:

```
1
```

La concatenación sería:

```
1 4 OP_ADD 5 OP_EQUAL
```

Y el resultado sería válido (`TRUE`).


### Firma digital ECDSA (*Elliptic Curve Digital Signature Algorithm*)

Las firmas digitales sirven para:

- Demostrar que el que firma es el dueño:
    - Autorización
    - No-repudio
- Demostrar que no ha habido alteraciones en los datos

> DETALLE: Los inputs se firman de forma independiente, cada uno con su
> `scriptSig`

El proceso de firma es el siguiente:

$$
Sig = F_{sig}( F_{hash}( m ), dA)
$$

Donde:  
$Sig$ = Firma  
$dA$ = Clave privada  
$m$ = Mensaje, las partes de la transacción a firmar  

La función $F_{sig}$ devuelve una pareja de puntos normalmente conocidos como
`R` y `S`:

$$
Sig = (R, S)
$$

Se serializan normalmente mediante DER (*Distinguished Encoding Rules*), un
formato que añade metadatos que facilitan la comprensión de la firma (longitud
de la secuencia, tipo de hash utilizado, etc.).

#### Verificación de la firma

Para verificar una firma es necesario disponer de la pareja de valores `R` y
`S`, la transacción serializada y la clave pública correspondiente a la clave
privada utilizada para firmar.

El algoritmo verificador devolverá `TRUE` o `FALSE` tras comprobar si la firma
es correcta.

##### SIGHASH

En Bitcoin los mensajes son la propia transacción. Como se puede firmar por
partes, el campo SIGHASH permite usos avanzados como multifirmas,
crowdfundings, colección de polvo, etc.

> NOTA: El campo SIGHASH también se añade al mensaje y se firma para que no
> pueda cambiarse.

El SIGHASH puede tomar tres valores principales:

- `ALL`
- `NONE`
- `SINGLE`

Cada uno de ellos implica qué partes de la transacción han sido firmadas por el
firmante, y cuales pueden cambiarse después. Una persona puede enviar una
transacción a otra habiendo firmado partes y dejándole a la otra firmar otras
partes e incluso añadir otros inputs o outputs. Una vez terminada la
transacción puede enviarse a la red para que se valide y se registre.

Además SIGHASH tiene algunos modificadores que aplican con una operación
binaria `OR` sobre el SIGHASH. Por ejemplo `ANYONECANPAY`, que firma sólo una
de las entradas y permite editar el resto.

[Más información](https://bitcoin.org/en/developer-guide#term-signature-hash)

#### La matemática de las firmas

El algoritmo crea un punto $P$ en la curva elíptica utilizando una clave
efímera $k$. Del punto $P$ se obtienen $R$ (de su coordenada $x$) y $S$ de la
siguiente forma:

$$
P = k \cdot G
$$
$$
R = coordenada\ x\ de\ P
$$
$$
S = k^{-1}(Hash(m) + dA \cdot R) \bmod p
$$

Donde:  
$k$ = una clave privada efímera  
$dA$ = clave privada de firmado  
$m$ = los datos de la transacción (el mensaje)  
$p$ = el orden de la curva elíptica  
$G$ = *generator point* de la curva elíptica

Entonces:
$$
P = S^{-1} \cdot Hash(m) \cdot G + S^{-1} \cdot R \cdot Q_a
$$

Donde:  
$Q_{a}$ = Clave pública asociada a la clave privada usada para la firma  

Si la coordenada $x$ de $P$ es igual a $R$, entonces la firma es correcta.

> WARNING: La clave efímera es importante que sea siempre diferente. Si se usa
> la misma clave efímera en dos ocasiones la clave privada puede exponerse.
> **No es una posibilidad teórica, ha ocurrido**.
>
> Para evitarlo se siguen estrategias como el RFC 6979 que usan datos del
> propio mensaje para generar $k$ desde ellos asegurando así que siempre sea
> distinta.


## Scripting avanzado

### Multifirma

Se pueden crear scripts que graban N firmas y requieren al menos de M firmas
para liberar los fondos

```
M <Pub key1> <Pub key2> ... <Pub keyN> N OP_CHECKMULTISIG
```

Y se liberan con:

```
<Sig 1> <Sig 2> ... <Sig M>
```

> NOTA: `CHECKMULTISIG` tiene un bug que obliga a introducir un `0` del script
> de unlock al principio porque coge un argumento de más por error. Ahora forma
> parte del consenso. **Es necesario hacerlo**

### Pay to script hash P2SH

En lugar de aportar el script completo se entrega el `HASH160` del script de
unlock. Así que el *locking script* se convierte en:

```
OP_HASH160 <Redeem script hash> EQUAL
```

El script sustituido por su hash se conoce como *redeem script*.

Y para desbloquearlo es necesario enviar el script completo.

```
firmas_necasarias <Redeem script>
```

Al comprobarlo se aplica el hash sobre el propio script así se comprueba si el
script introducido era el adecuado. Una vez comprobado que el *redeem script*
que se buscaba era ese, se ejecuta el *unlocking script* para asegurarse de que
es capaz de desbloquear el *redeem script*.

##### Direcciones P2SH (BIP-13)

Usan el prefijo `5` que al pasarlo por Base58-Check se convierte en `3`.

#### Características del P2SH:

- Transacciones con menos datos (menores tasas)
- El pago se realiza de forma normal
- La complejidad pasa al cobrador
- Se usa el script largo al cobrar, no al pagar
- El gasto de transacción se traspasa al cobrador

> NOTA: Independientemente de que el hash del script sea válido o no, siempre
> será aceptado porque no hay ninguna forma de saber cuál es el script en el
> momento de la inserción. Si se construye mal, los Bitcoin se perderán.


### Guardar datos en el Blockchain: `RETURN` script

Existe un script específico para guardar datos eque no se procesan como UTXO y
no se almacenan en memoria RAM pero sí en el Blockchain.

> NOTA: Los UTXO suelen almacenarse en RAM para verlos más rápidamente.

```
RETURN <DATA>
```

Este script acepta hasta 80 Bytes en su campo `DATA`. Al ejecutarse siempre
devuelve `FALSE` y el script falla automáticamente. **Los fondos introducidos
se pierden**.

El uso de este script entiende el blockchain como una base de datos en la que
escribir tiene un coste. Suelen usarse para guardar hashes con un prefijo
adicional para indicar la aplicación. Por ejemplo: Proof-Of-Existance.

### Timelocks

Los bloqueos de tiempo sirven para añadir la dimensión temporal a los scripts
de Bitcoin. Añadiendo esta posibilidad se habilitan usos avanzados. Existen
varios Timelocks diferentes en Bitcoin.

#### Transaction locktime: `nLocktime`

`nLocktime` es el primer *timelock* definido en Bitcoin. Es un bloqueo temporal
por transacción. Se interpreta de dos maneras:

1. Si su valor está entre `0` y `500 millones`: Se refiere al *Block Height*
   [^2]
2. Si su valor es mayor que `500 millones`: Se refiere al Unix Timestamp

Si no se ha superado el *Block Height* o el tiempo indicado, la transacción ni
se acepta ni se reenvía. Las transacciones con un `nLocktime` deben enviarse a
la red una vez el `nLocktime` haya expirado.

> WARNING: Nadie asegura que en el tiempo entre la creación de la transacción y
> el momento de ejecutarla no se hayan gastado los fondos a los que la
> transacción hace referencia. Para esto es mejor usar otro método que bloquee
> los UTXO.

[^2]: Como se verá más adelante el Block Height se refiere a la posición del
  bloque en el Blockchain respecto al Bloque génesis.

#### *Check Lock Time Verify* (*CLTV*) BIP-65

Se trata de un timelock por *output* de la transacción. Sólo permite gastar el
output una vez el tiempo haya expirado.

Es un código de scripting, una *nueva orden*: `OP_CHECKLOCKTIMEVERIFY`

Recibe un input con la misma forma que el `nLocktime`. Si el
`OP_CHECKLOCKTIMEVERIFY` falla termina la ejecución del script.

#### Timelocks relativos: BIP-68 y BIP-112

Los timelocks relativos permiten gastar los *outputs* de una transacción
después de haber pasado un tiempo desde el registro de la transacción.

##### Relative lock-time (*RTL*) via `nSequence`: BIP-68

`nSequence` fue creado con la intención de sobreescribir una transacción en el
mempool con otra nueva con el mismo input y un número de secuencia mayor. Esto
nunca ha funcionado correctamente. Este BIP propone una redefinición del campo
`nSequence` para nuevos casos sin romper la funcionalidad previa y permite
futuras expansiones.

Si `nSequence` < 2³¹ se interpreta que la un timelock relativo que sólo permite
gastar el UTXO cuando han llegado `nSequence` bloques nuevos al Blockchain
desde la aparición de la transacción.

Además, el BIP define más campos dentro del `nSquence` para otros usos.

Igual que con los anteriores, es posible indicar un modo temporal o un modo
basdo en el número de bloques.


##### Check Sequence Verify (*CSV*): BIP-112

Es un comando de script (`OP_CHECKSEQUENCEVERIFY`) que sólo permite gastar el
UTXO de una transacción que tiene un `nSequence` mayor o igual al CSV.
Básicamente restringe el gasto de UTXOs hasta que haya pasado un tiempo o se
hayan registrado un número de bloques desde que ese UTXO fue minado.

Es una forma de aplicar el `nSequence` en el script.

Es interesante porque obliga a que la transacción anterior haya sido minada y
haya pasado un tiempo desde su minado para ejecutar la nueva transacción.

#### Median-Time-Past: BIP-113

El contexto descentralizado de Bitcoin crea pequeñas diferencias horarias entre
los nodos. Para controlar este tipo de desviaciones se define el BIP-113.

Este BIP defien un nuevo modo de cálculo temporal para los casos anteriores que
decide si las transacciones son validas en función del valor mediano de los
timestamps de los 11 bloques anteriores, ya que es más robusto que el timestamp
del bloque de la transacción. El consenso asegura que este valor crece de forma
monótona y dificulta que los mineros mientan a la hora de introducir los
timestamps de bloque.

#### Fee-sniping

El *fee-sniping* es un tipo de ataque en el que los mineros intentan volver a
minar los bloques ya minados para seleccionar transacciones con las tasas más
altas (entre el bloque y el mempool).

Este comportamiento no es muy lucrativo en la actualidad pero cuando los
premios del minado bajen será mucho más lucrativo.

Para evitarlo, Bitcoin Core usa un `nLocktime` igual a la posición del bloque
actual +1. Fuerza que minen las nuevas transacciones se minen en bloques
nuevos.

### Flow control

Se pueden crear scripts que tengan más de un modo de desbloqueo. La forma
`IF...ELSE...ENDIF` se encarga de esto. Debido a que es un lenguaje basado en
el stack, tienen esta forma:

```
condition
IF
    code to run when condition is true
ELSE
    code to run when condition is false
ENDIF
code to run later
```

> NOTA: Fijarse que la condición es antes que el `IF`

Para controlar las condiciones existen varios operadores como `BOOLAND`,
`BOOLOR` y `NOT`.

La sentencia `VERIFY` sirve para comprobar una condición y en función de ella
sigue la ejecución (si es `TRUE`) o el script termina con un fallo (si es
`FALSE`).

## La red de Bitcoin

Se refiere a la colección de nodos que hablan el *protocolo* Bitcoin.

### El nodo *con todo*

La implementación de referencia *Bitcoin Core* forma un nodo con los elementos
lógicos necesarios para definir el funcionamiento de Bitcoin.

Consta de 4 elementos lógicos principales:

- **Cartera** (*wallet*): la cartera gestiona las claves y direcciones para
  crear transacciones.
- **Minero** (*miner*): El minero resuelve el algoritmo Proof-of-Work.
- **Cadena de bloques** (*blockchain*): Significa que el nodo guarda una copia
  completa del blockchain. No todos los nodos lo hacen.
- **Enrutador de red Bitcoin** (*bitcoin network router*): Todos los nodos
  incluyen de algún modo un módulo de conexión a la red. Cuidado: no todos
  hablan necesariamente el protocolo bitcoin, existen otros.

### Red extendida

La red de Bitcoin hoy en día se nutre de otros protocolos y combina diferentes
elementos del nodo *con todo* para crear nodos con funcionalidades específicas.

Además de la implementación de referencia llamada *Bitcoin Core* hay otras
implementaciones y cada una tiene su propia forma de actuar.

Existen protocolos adicionales como *Stratum* que necesitan *gateways* para la
intercomunicación con el protocolo Bitcoin.

Nodos más comunes:

- *Full node*: Network router + Blockchain
- *Solo miner*: Miner + Blockchain + Network router
- *Lightweight (SPV) wallet*: Wallet + Network router
- *Pool protocol server*: Pool server + Stratum server
- *Mining Nodes*: Miner + Pool / Stratum
- *Lightweight (SPV) Stratum wallet*: Wallet + Stratum

### Relays

Bitcoin es una competición de velocidad por lo que los retrasos debidos a la
red suponen un problema. Para facilitar la sincronización y que los nuevos
bloques y transacciones lleguen antes a toda la red existen redes de *relays*.
Históricamente, de antiguo a moderno:

- Bitcoin Relay Network
- Bitcoin Fibre
- Falcon

### Network Discovery

Dos métodos principales:

1. DNS Seeds: Bitcoin Core tiene *hardcoded* la dirección de 5 DNS seeds que
   entregan direcciones de nodos conocidos al consultarles. Opción `dnsseed`
   para activar (por defecto) o desactivar el comportamiento en Bitcoin Core.

2. IP de un nodo conocido: Puede utilizarse un nodo conocido para que éste nos
   presente al resto. Opción `seednode` en el Bitcoin Core.

El proceso de búsqueda está siempre activo ya que no se espera que las
conexiones aguanten.

Los nodos de bitcoin lanzan mensajes *keep-alive* a los nodos conocidos, si
llevan más de 90 minutos sin hablarse se considera que la conexión se ha
perdido.

### Nodos

Existen dos tipos principales de nodos, los que mantienen una copia completa
del Blockchain y los que no.

#### Full nodes

Deberían llamarse *full-blockchain* porque no tienen todas las funcionalidades,
pero sí un Blockchain completo.

Estos nodos **pueden verificar transacciones sin necesitar acceso a otros
nodos**, ya que tienen su copia del Blockchain.

> NOTA: el 75% de los *full-node*s de la red son Bitcoin Core (la
> implementación de referencia. Se identifican a sí mismos como `Satoshi` al
> enviarles usar `getpeerinfo`. *¿Por qué no probarlo como ejercicio?*

Lo primero que hacen cuando se encienden es descargar el Blockchain completo.
Para eso usan el mensaje `inv`. El proceso es el siguiente:

1. Se conecta el nodo a la red y descubre al resto
2. Envía un mensaje `getblocks` al nodo descubierto (el otro nodo también lo
   hace, ya que no sabe si éste tiene bloques que él desconoce). El mensaje
   `getblocks` incluye qué bloque es el último de la cadena.
3. El nodo con más bloques incluye un resumen de hasta un máximo de 500 bloques
   en un mensaje `inv` (*Inventory*) y lo envía al otro nodo.
4. El nodo receptor del mensaje `inv` pide los bloques con el mensaje
   `getdata`.
5. El nodo con más bloques los envía ordenadamente uno a uno hasta mandar los
   500 y vuelven al paso 2.

Este proceso de entregar los bloques en pequeños conjuntos ayuda frente a
fallos en la red y evitan que los nodos se ahoguen por cantidad de mensajes.

Este proceso ocurre siempre que un nodo se desconecta de la red y vuelve.

> NOTA: los 500 bloques son un número configurable.

#### Nodos SPV (*Simplified Payment Verification*)

Los nodos SPV han sido diseñados para entornos en los que no se puede admitir
el coste de almacenar un Blockchain completo.

Estos nodos sólo almacenan las cabeceras de los bloques (*block headers*).
**No almacenan transacciones**.

**Confían**, por tanto, en un *full-node*.

Su proceso de confirmar las transacciones es el opuesto al de un full-node. Los
SPV las confirman *por arriba* mientras que los full nodes lo hacen *por
abajo*[^3]:

- Los nodos completos comprueban a la llegada de un nuevo bloque o transacción
  si son correctos, comparando con *su* copia del Blockchain. Comprueban la
  coherencia con el *pasado*, con los bloques que están *por debajo* de la
  transacción o el bloque entrante.

- Los nodos SPV comprueban a *futuro*, mirando si los nodos completos han
  añadido nuevos bloques sobre la transacción o bloque que quieren verificar.
  Entienden que si se colocan más bloques sobre ese es porque no ha habido
  doble gasto. Comprueban por tanto, lo que queda *por arriba*.

[^3]: En esta metáfora se representa el blockchain como un conjunto de bloques
  apilados, siendo el primer bloque el bloque inferior.

El proceso de obtención de inventario es diferente para los nodos SPV, ya que
ellos sólo requieren las cabeceras, aunque en realidad es casi idéntico.
Utilizan el mensaje `getheaders` y en lugar de 500 por defecto reciben 2000.

Si los nodos SPV pidiesen transacciones individuales podrían ser fácilmente
engañados por nodos maliciosos o sus comportamientos podrían ser estudiados
para conocer las direcciones en las que el nodo SPV se interesa. Para evitar
esto se utilizan *bloom filters*.

##### Bloom filters BIP-37

Son un modo de describir un patrón sin necesidad de especificarlo de forma
exacta.

El nodo SPV muestra lo que le interesa en mediante un *bloom filter* así que
los nodos completos no saben específicamente lo que interesa al nodo SPV. El
nodo SPV recibe más información de la que necesita, pero es capaz de filtrarla
él mismo.


##### Privacidad en los nodos SPV

Los nodos SPV son menos privados que un *full node*, incluso con los *bloom
filters* pueden escucharse y buscar patrones. Si se requiere privacidad se
recomienda el uso de un nodo completo ya que éste puede analizar las
transacciones que quiera en su propio Blockchain sin dejar rastros.

### Encriptación y autenticación

En Bitcoin los mensajes viajan sin cifrar. Los BIP 150 y 151 aportan
encriptación y autenticación.

> WARNING: realizar ataques *man-in-the-middle* es relativamente sencillo.

Bitcoin puede usar la red Tor para operar. Bitcoin Core puede ejecutarse como
*hidden service*.

### Transaction pools

- Los nodos guardan las transacciones válidas pero no confirmadas en su
  memoria. Se conoce como *mempool*.
- Algunas implementaciones disponen de otra memoria para las transacciones
  huérfanas.

Ambos *pool*s varían mucho de un nodo a otro ya que dependen de su estado y su
contexto cercano. **No se puede confiar en ellos**, son una simple forma de
mantener las transacciones conocidas. Si los nodos se reinician, al ser datos
almacenados en memoria, se vacían.

También hay nodos que disponen de un *UTXO pool* de rápido acceso a los
*output* no gastados. A diferencia de los otros pools descritos anteriormente,
éste está basado en el consenso porque está formado por transacciones
confirmadas.



## El Blockchain

Es una estructura de datos *back-linked*.

Se puede guardar de muchas formas (un archivo en disco, una base de datos...).
Bitcoin Core usa LevelDB.

> NOTA: LevelDB es una base de datos key-value de rápido acceso.

### Estructura del Bloque

- Magic no (4 bytes) = `0xD9B4BEF9`[^4]
- Block Size (4 bytes)
- Block header (80 bytes)
- Transaction count (1-9 bytes)
- Transactions (variable)

[^4]: Los magic number son un patrón usado ampliamente en la informática. Más
  información:
  [https://en.wikipedia.org/wiki/Magic_number_(programming)](https://en.wikipedia.org/wiki/Magic_number_(programming))

#### Estructura del Block Header

- Version (4 bytes)
- Previous block hash (32 bytes)
- Merkle Root (32 bytes)
- Timestamp (4 bytes)
- Difficulty target (4 bytes)
- Nonce (4 bytes)

#### Identificadores de bloque: *block-hash* vs *block-height*

El ID del bloque se obtiene con un *double-SHA256* del *header*. Se conoce como
*block-hash* (pero se hace sólo en la cabecera). Suelen guardarse por separado
para tener acceso rápido.

El *block-height* se refiere a la posición del bloque en la cadena respecto al
*bloque génesis* (posición `0`). Cuenta el número de saltos a realizar desde el
bloque hasta llegar al génesis. El *block-height* no es un indicador único, si
existen *forks*, el mismo *block-height* puede hacer referencia a más de un
bloque.

#### Bloque génesis

El bloque génesis (*genesis block*) es el primer bloque del Blockchain.

Está *hardcoded* en el código del nodo. **Todos los nodos lo conocen**.

Tiene un mensaje oculto en los input de la transacción *coinbase*:

```
The Times 03/Jan/2009 Chancellor on brink of second bailout for banks
```

[Más información](https://en.bitcoin.it/wiki/Genesis_block)

#### Merkle trees

En bitcoin los *Merkle trees* resumen las transacciones y sirven a la vez para
mantener la integridad de los bloques.

Usan *double-SHA256* para obtener los hashes.

Cada transacción se *hashea* de la siguiente manera:

![Estructura de un Merkle Tree. Imagen obtenida de Wikipedia](img/merkle-tree_wiki.png)

$$
H_i = SHA256( SHA256( Tx_i))
$$

Y los *hashes* se procesan por parejas de la misma forma hasta llegar a un
único hash:

$$
H_{ij} = SHA256( SHA256( H_i + H_j))
$$

> NOTA: Si el número de transacciones es par se repite la última.

Para comprobar que una transacción está en el bloque sólo se necesita el
*Merkle Path* y realizar $\log_{2}{N}$ hashes.

> NOTA: Los Merkle trees se utilizan en infinidad de aplicaciones: Git,
> Torrent, IPFS, Dat...

##### Merkle trees en SPV

Como los nodos SPV no disponen de las transacciones, necesitan algún método
para asegurarse de que pertenecen a un bloque. Para ello utilizan un *Merkle
path*.

![Merkle path. Imagen tomada del libro *Mastering Bitcoin*](img/merkle_path.png)

Si un nodo SPV está interesado, por ejemplo, en las transacciones entrantes a
una dirección establecerá un *bloom filter* en la conexión con los nodos
completos a los que esté asociado para limitar el conjunto de transacciones a
recibir a las de la dirección de interés. Cuando un nodo vea una transacción
que cumple el *bloom filter* la enviará al nodo SPV usando el mensaje
`merkleblock`, que contiene la cabecera (*header*) del bloque y el *merkle
path* necesario para para que el nodo SPV pueda certificar que la transacción
forma parte del bloque.

De esta forma, el nodo SPV es capaz de mantener su blockchain local formado por
*headers* y al mismo tiempo comprobar que las transacciones forman parte de los
bloques sin necesidad de tener el registro de todas las transacciones.

Esto reduce la cantidad de datos necesarios en un nodo SPV aproximadamente a la
milésima parte.

### Otros Blockchains

- `mainnet`: Red de producción
- `testnet`: Red de pruebas. Intenta mantener una dificultad baja, pero no
  siempre es posible porque también se prueban *ASIC*s de minería en ella. De
  vez en cuando se reinicia.
- `regtest`: Red de tests de regresión. Se refiere a una red local que los
  usuarios preparan para probar sus aplicaciones en un entorno controlado. No
  hay una global, cada uno crea la suya.
- `segnet`: Red de pruebas para *SegWit*.

El desarrollo de aplicaciones de Bitcoin nuevas requiere de pruebas ordenadas.
Se comienza probando en `regtest` y cuando se ha depurado la aplicación se
ejecuta en `testnet` donde interactúa con una red completa durante un tiempo.
Una vez se está seguro de que puede pasar a producción se mueve al `mainnet`.
Es necesario ser minucioso en este proceso porque los fondos de los usuarios
están en juego y hay otros equipos involucrados en la red.


## Minado y consenso

El minado se conoce como *minado* (*mining*) por el hecho de que obtiene nuevas
reservas de Bitcoin, como si en un modelo económico basado en el oro se
extrajese oro de una mina. La realidad es que la obtención de nueva moneda es
la parte menos importante del proceso de minado, ya que éste es principalmente
una pieza para llegar al consenso distribuido.

### Creación de nuevas reservas

Los mineros que crean bloques reciben un incentivo que consta de:

- Las tasas (*fees*) de las transacciones de todo el bloque
- Un premio (*reward*) que aumenta la cantidad de bitcoins en circulación.
    - Cada 210,000 bloques se reduce a la mitad.
    - Comenzó en 50 Bitcoins ($50 \cdot 10^{8}$ satoshis)
    - Es el minero quien decide su premio. Puede decidirse recibir menos
      premio, este límite es el superior, el que otros nodos no aceptarán.

Este proceso de reducción del aumento que la cantidad de Bitcoins sufre hace
que Bitcoin sea una moneda **deflacionaria**, lo que significa que Bitcoin
tiene más poder de consumo a medida que el tiempo pasa. Al contrario que la
moneda tradicional, cuyo valor se devalúa a medida que pasa el tiempo
(inflacionaria). Se considera en la moneda tradicional que la *deflación*
es el resultado de una catástrofe, ya que implicaría que la economía no crece,
sin embargo, en Bitcoin la deflación es una decisión de diseño que aporta otro
modelo económico al que no estamos acostumbrados.

### Consenso descentralizado

El verdadero invento en Bitcoin es el **consenso descentralizado**. El consenso
de Bitcoin se puede decir que es un **consenso emergente** ya que no se alcanza
de forma explícita.

Estos puntos crean el consenso:

1. **Verificación independiente de las transacciones** por cada nodo completo.
2. **Agregación de las transacciones en bloques minados por con un PoW** basado
   en poder de computación.
3. **Verificación independiente de cada bloque** por cada nodo completo.
4. **Selección independiente de la cadena con más PoW acumulado** por cada nodo
   completo.


#### Verificación independiente de las transacciones

Las carteras usan los UTXO, crean transacciones y las mandan a la red para que
se propaguen. Antes de propagarlas, los nodos las comprueban.

Se realizan comprobaciones de diversos tipos (que las transacciones no son
`coinbase`, que la sintaxis y los datos son correctos, etc). Estas reglas
varían a medida de que el consenso va variando, pero se almacenan en las
funciones del Bitcoin Core:

- [`AcceptToMemoryPool`](https://github.com/bitcoin/bitcoin/blob/72e358dca7d9f470e9e42e4b715a8edfc7b53bdb/src/validation.cpp#L985)
- [`CheckTransaction`](https://github.com/bitcoin/bitcoin/blob/78dae8caccd82cfbfd76557f1fb7d7557c7b5edb/src/consensus/tx_verify.cpp#L159)
- [`CheckInputs`](https://github.com/bitcoin/bitcoin/blob/72e358dca7d9f470e9e42e4b715a8edfc7b53bdb/src/validation.cpp#L985)

> NOTA: Los enlaces de las funciones se añaden como referencia para demostrar
> que es fácil leer las condiciones. Por desgracia, están asociados a un commit
> concreto, en el futuro se recomienda analizar la versión en producción ya que
> estos enlaces pueden quedar obsoletos.

Cada transacción validada pero no confirmada (no forma parte de un bloque) se
guarda en el *mempool* (*Memory Pool*, *transaction pool*, etc)

#### Agrupación de transacciones en bloques: El proceso de minado

Tras validar las transacciones, añadirlas al *mempool* y extenderlas por la
red, el nodo (minero) las agrupa en un bloque candidato (*candidate block*) y
trata de resolver el PoW (*Proof of Work*) para ese bloque.

El proceso a seguir es el siguiente:

1. Se añaden las transacciones
2. Se añade la transacción *coinbase* como primera transacción
3. Se prepara la cabecera del bloque (*block header*)
4. Se procesa el Proof-of-Work

Si recibe un bloque que ocupe la posición del que él busca significa que no ha
ganado la carrera así que debe preparar otro bloque candidato. Para esto,
tomará las transacciones que el bloque ganador no haya añadido, tanto de su
`mempool` o del bloque candidato que propuso, y las agrupará en un nuevo bloque
candidato. Las transacciones que sí aparezcan en el bloque ganador deberá
descartarlas porque ya han sido registradas.

Si consiguiera ganar el PoW emitiría el bloque a todos sus *peers* para que lo
anunciasen por la red. Para saber que ha ganado, basta con no haber recibido
ningún bloque en esa posición de ninguno de sus *peers*.

##### La transacción *coinbase*

La primera transacción de los bloques es la transacción *coinbase*. Esta
transacción la crean los mineros y es donde registran su incentivo.

Es el único tipo de transacción que no gasta UTXO.

El premio se calcula de la siguiente manera:

$$
CoinbaseOutput = Fees + Reward
$$
Donde:
$$
Fees = Sum( Input ) - Sum( Output )
$$
$$
Reward = 50 \cdot \frac{10^8}{2^{\lfloor\frac{BlockHeight}{210000}\rfloor}}
$$

> NOTA: En Bitcoin Core el *reward* se conoce como `Subsidy` y su cálculo puede
> comprobarse en la función `GetBlockSubsidy`.

El cálculo de las tasas se realiza sumando la diferencia entre inputs y outputs
de todas las transacciones incluidas en un bloque.

El *reward* se obtiene del *reward* original (50 Bitcoin), convertido a
*satoshi* (multiplicado por la constante `COIN`= 10⁸) y dividiendo por 2 un
número de veces. Como se adelantó, cada 210,000 bloques (*halving interval*) el
premio se reduce, por tanto es necesario obtener el número de veces que se ha
superado la cifra del *halving interval*. Para ello, se divide el *block
height* entre el *halving interval* y se redondea hacia abajo.

Este cálculo tiene un límite: cuando la división entre el *block height* y el
*halving interval* es mayor que 64, el *reward* pasa a ser 0.

La transacción *coinbase* tiene un formato distinto al resto de transacciones
debido a que ésta no necesita el campo *input*. Esta peculiaridad ha hecho que
el campo input se extienda de la siguiente forma:

- `outputIndex` siempre es `0xFFFFFFFF`
- `txHash` siempre es `000...0` y ya no hace referencia a ninguna transacción
- *Unlocking script size* pasa a ser *Coinbase data size*
- *Unlock script* pasa a ser *Coinbase data*
- `seqNumber` es `0xFF..FF`

El *unlocking script* se utiliza para introducir datos. Varios BIP definen la
introducción de datos de versión o el *block height* en la transacción
*coinbase* afectando a los primeros bytes del campo. El resto están disponibles
para introducir **datos arbitrarios**. Se usan principalmente como:

- Datos del *mining pool*
- Extra *nonce*

##### El *block header*

Éste es el proceso de creación de la cabecera del bloque:

1. Seleccionar la versión del bloque
2. Seleccionar el `prevBlockHash` que indica sobre qué bloque se añadirá éste
   en la cadena
3. Obtener el `Merkle Root` de las transacciones
4. Introducir el `timestamp`, el [UNIX
   timestamp](https://en.wikipedia.org/wiki/Unix_time) (4 bytes)
5. Marcar el `difficultyTarget`: (1 byte de exponente y 3 de mantisa)
6. Inicializar el `nonce` a 0 y comenzar el minado

##### Algoritmo Proof-Of-Work

El objetivo del *Proof-of-Work* es encontrar el *hash* de la cabecera que sea
menor al *difficulty target*. Donde:
$$
difficultyTarget = mantissa \cdot 2^{8 \cdot (exponent - 3)}
$$

Los bloques se generan cada 10 minutos de media, para conseguir eso es
necesario cambiar la dificultad.

Cada `2016` bloques se compara el tiempo que se tardó en minarlos y la nueva
dificultad se calcula de ello:
$$
NewTarget = OldTarget \cdot \frac{Time\ Last\ 2016\ Blocks}{2016\ blocks \cdot
10\ minutes}
$$

Como referencia temporal: 2016 bloques minados cada 10 minutos tardan 2
semanas.

Para evitar volatilidad en la dificultad, si el valor de *retargeting* es mayor
que 4 se mantiene en 4.

Una vez definida la dificultad, el minero calcula *double-SHA256* del *block
header* y comprueba si el valor es menor a la dificultad. Si lo es, ha
superado el PoW. Si no lo es, cambia el valor del campo *nonce* y vuelve a
intentarlo.

#### Verificación independiente de los bloques

Si el bloque supera el PoW se envía a los *peers*. Éstos hacen una serie de
comprobaciones iniciales y si el bloque las supera lo reenvían a sus *peers*.
Las comprobaciones pueden leerse en las funciones `CheckBlock` y `CheckBlockHeader`
de Bitcoin Core pero son las siguientes:

- Que el bloque sea sintácticamente válido
- Que el *block header hash* < *difficulty target*
- Que el *timestamp* < dos horas en el futuro
- Que el tamaño del bloque < tamaño máximo de bloque
- Que la primera, y sólo la primera, transacción sea una transacción *coinbase*
- Que todas las transacciones sean correctas

Esta validación asegura que los mineros no pueden engañar al resto. Si pusieran
transacciones inválidas los nodos las descartarían.

#### Selección de la cadena con más PoW acumulado

Los nodos mantienen tres grupos de bloques:

1. La cadena principal (la más larga)
2. Las ramas de la cadena
3. Un *pool* de bloques huérfanos

Al recibir un nuevo bloque los nodos comprueban su `prevBlockHash` y lo buscan
en sus grupos de bloques para situarlo. La idea es que el bloque sea el último
de la cadena principal, pero esto no es siempre así.

Si el bloque recibido es válido pero no se encuentra su bloque anterior, se
guarda en el grupo de huérfanos. Se entiende que su padre no ha llegado aún por
un problema en la red. Si su padre apareciera se ordenarían y si fuera
necesario se introducirían en la cadena principal o una de sus ramas.

Si se recibe un bloque que está conectado a la cadena pero no es el último, se
guarda como una rama. Esto proceso se conoce como **fork**. La cadena principal
por tanto puede no ser sólo una porque durante ciertos periodos de tiempo puede
tener varias cabezas, este proceso se termina resolviendo con el voto basado en
potencia de computación.

Los nodos siempre minarán bloques para añadirlos a su cadena principal, pero es
posible que su cadena principal diverja del resto.

##### Forks

Como el blockchain está distribuido es posible que sus copias no sean
coherentes. Cuando dos mineros producen bloques distintos en un corto periodo
de tiempo pueden ocurrir inconsistencias.

- Desde el punto de vista del blockchain ambos son correctos así que no pueden
  descartarse. Se añaden de forma paralela.

- Normalmente estos **forks** se resuelven con un solo bloque adicional ya que
  se deben a un problema puntual.

> NOTA: Los 10 minutos de tiempo para crear nuevos bloques son una decisión de
> diseño clave. Es un tiempo suficientemente grande para limitar la ocurrencia
> de estos sucesos y suficientemente bajo para tener confirmaciones rápidas.


### Minado en detalle

El minado es un proceso complejo debido a que la red ha cambiado mucho desde su
concepción, existen varios detalles hoy en día que afectan radicalmente al
proceso.

#### Capacidad de minado

Debido al uso de GPUs y posteriormente de ASIC (*Application Specific
Integrated Circuit*) para el minado la capacidad de minado ha crecido
exponencialmente.

Hoy en día la dificultad es tal que el valor del *nonce* se agota en segundos y
se necesitan más valores variables para acertar el PoW. Durante un tiempo se
utilizó el campo *timestamp* del bloque para obtener varios valores
adicionales.

> NOTA: Esto es posible porque los nodos son bastante laxos en la comprobación
> del timestamp, como se comenta en el apartado de verificación de los bloques.
> Esto se debe a que las características de la red exigen cierta flexibilidad
> en los tiempos.

Hoy en día se utiliza el campo de datos de la transacción *coinbase* (de 2 a
100 bytes) como un *nonce* extra.

> NOTA: Aunque sólo se calcule el hash del *block header* a la hora de minar,
> el *block header* contiene el *Merkle Root* de las transacciones. Alterar el
> valor de la transacción *coinbase* altera el *Merkle Root* y con ello el hash
> del bloque.

#### Mining Pools

Los *mining pools* son un sistema de coordinación para facilitar el acierto del
PoW con el que varios nodos acuerdan repartirse las ganancias obtenidas en
función del trabajo aportado por cada uno.

Para calcular la aportación de cada nodo se reduce la dificultad a la que los
participantes minan. De este modo, los participantes creen acertar más a menudo
y envían sus bloques al *mining pool* con mayor frecuencia. El *mining pool*
sólo tiene que contar el número de bloques aportado por cada uno y filtrarlos
hasta obtener uno que cumpla la condición real. Se entiende que los nodos que
han aportado más bloques que cumplen la condición laxa es porque han hecho más
comprobaciones, por lo que aportan más trabajo al equipo. Las ganancias
obtenidas se reparten en consecuencia.

##### Managed pools

Disponen de un *pool operator* centralizado con software específico instalado
para gestionar la *pool*.

Usan protocolos de minado específicos:

- Stratum (STM)
- GetBlockTemplate (GBT)

El proceso:

1. Crean una plantilla de bloque (*block template*) y la reparten entre los
   participantes
2. Los participantes cambian el *nonce* y calculan el hash para pasar el PoW de
   menor dificultad

Los participantes de estas redes no necesitan ser nodos completos ni comprobar
las transacciones ni bloques, se comportan como un cluster de computación
creado para superar el PoW. En realidad el único que conoce su aplicación es el
*pool operator*.

##### P2P pools (*P2Pool*)

Los *managed pools* centralizan la red y eso puede acarrear problemas:

- Permite a los *pool operator*s realizar double-spends si consiguen suficiente potencia de computación
- Suponen un *single point-of-failure*

Los *P2Pools* descentralizan los *mining pools* usando un blockchain paralelo
de menor dificultad llamado *share chain* pero el proceso es equivalente. El
*share chain* sirve para dejar registro de la capacidad de trabajo de cada
nodo.

- Producen bloques cada 30 segundos
- Es necesario que todos sean *full nodes* y que, además, instalen el software
  *P2Pool*

### Consenso en detalle

Existen ocasiones en las que el consenso se ve alterado y es necesario
conocerlas para ser conscientes de las debilidades del consenso y de el efecto
que los cambios de consenso pueden tener en la red.

#### Ataques de consenso

Si muchos mineros (o uno con un poder de computación colosal) vota con fines
deshonestos puede transformar el consenso.

En teoría un fork puede ocurrir en cualquier *block height* pero el poder de
computación necesario para resolverlo hacia su lado es el poder acumulado de
todos los bloques superiores.

Con un poder de computación alto puede atacarse la red de a sabiendas de que se
puede decidir en qué rama del fork se va a conseguir la cadena más larga. El
minero malicioso puede gastar fondos en una rama y al recibir la mercancía
relacionada crear una nueva rama anterior para recuperar los fondos y minar
hasta que ésta se convierta en la rama más larga.

> NOTA: Para defenderse de un ataque como este se recomienda esperar al menos
> hasta 6 confirmaciones en caso de hacer ventas de alto valor (1 hora). Se
> entiende que cualquier bloque enterrado por más de 6 bloques en el blockchain
> es imposible de reemplazar. Para ventas especialmente elevadas suelen
> esperarse 24 horas (unos 144 bloques).

Este ataque se conoce como ataque del 51% porque se supone que con más de la
mitad del poder de computación de la red es extremadamente factible realizar
este ataque.

En la práctica este ataque no requiere el 51% del *hashing power*, con el 30%
es factible realizarlo.

#### Hard forks

Al cambiar las reglas del consenso el un fork puede perpetuarse en el tiempo.
Razones:

- Nuevas implementaciones
- Cambios de criterio en nuevas versiones
- Otras

Ocurren cuando unos nodos no aceptan los bloques como válidos. Este tipo de
forks no se puede esperar que se resuelvan solos en el corto plazo porque
responden a un cambio de criterio en la red.

> NOTA: Cuando ocurren hard-forks los mineros eligen en qué rama de la cadena
> quieren minar. El *hashing power* se ve dividido en cada rama, lo que provoca
> que los próximos bloques tarden mucho en minarse.  
> **¡Peligro!** Tarda mucho tiempo hasta que la dificultad se revisa.  
> ¿Cuál es la alternativa? ¿La deuda técnica?

#### Soft forks

Sólo los cambios de consenso *no-backwards-compatible* generan un *hard fork*.
Los *soft forks* son cambios compatibles con lo que no provocan un *fork*.

> NOTA: Los soft forks es que no pueden ampliar las reglas de consenso, porque
> si así fuera los nodos rechazarían los bloques nuevos, sólo pueden
> constreñirlas.

##### Redefinición de Opcodes NOP

Script inicialmente partía con 10 opcodes para redefinir en el futuro (NOP1,
NOP2...). Los nodos antiguos interpretan el NOP como un comando inocuo, por lo
que los bloques no se invalidan, mientras que los nuevos nodos pueden utilizar
su redefinición.

Por ejemplo `CheckLockTimeVerify` ocupa el `NOP2`, tal y como se define en el
BIP-65.

##### Más métodos de Soft Fork

*SegWit* se propuso como un cambio a aplicar en un hard fork pero finalmente
usa una adaptación de las UTXO para que los nodos antiguos lo vean como válido.


##### Crítica a los soft forks

- Limitar el desarrollo por tratar de evitar un hard fork puede arrastrar una
  deuda técnica grave.
- Relajan la validación porque los nodos antiguos no son capaces de seguir la
  validación real. En cierto modo se les engaña.
- Las actualizaciones son irreversibles. Pueden perderse fondos si se vuelven
  atrás.

##### Soft forks con señalización

Existen sistemas de señalización para acordar la adopción de un soft fork como
el BIP-34 que posteriormente fue sustituido por el BIP-9.

###### Señalización por versión de bloque: BIP-34

El BIP-34 fuerza el uso del *block height* en la transacción *coinbase*. Para
activarlo, los nodos cambiaron su versión de bloque a 2. La transición fue
gradual. Se comprobaban los 1000 últimos bloques y se tomaban decisiones según
el resultado:

- Si el 75% de los bloques tenían el *version number* cambiado a 2, se les
  requería tener el *block height* en la transacción *coinbase*, pero a los que
  mantenían la versión 1 no se les exigía.

- Si eran el 95% de los bloques, los bloques con versión 1 eran rechazados.

Este proceso funcionó de forma exitosa y se aplicó de nuevo en el BIP-65
(versión 3) y BIP-66 (versión 4).

###### Señalización por versión extendida: BIP-9

El BIP-34 tiene unas limitaciones:

- Requiere hacer los soft forks de forma coordinada porque sólo hay un campo de
  versión en los bloques.
- No hay forma real de rechazar la transición y, aunque se hiciera, si se quiere
  aceptar un soft fork posterior al aumentar el número de versión se acepta
  cualquier soft fork intermedio.
- Cada cambio reduce el número de cambios futuros posibles.

BIP-9 usa el campo de versión a modo de cambio binario en lugar de como campo
numérico. Contando las versiones ya cerradas por el formato del BIP-34, quedan
libres 29 propuestas paralelas.

Además, el BIP-9 no fuerza el campo a estar activado para siempre: sólo se
utiliza durante un periodo de adaptación para mostrar conformidad y puede
usarse más adelante. Si no se ha activado durante un periodo de adaptación
(`TIMEOUT`) se considera rechazado.

> NOTA: De todas formas, para evitar problemas al reutilizar los campos,
> intentan usar todos antes de comenzar a reutilizar.

El BIP propone un sistema de tiempos basado en el Median Time Past, y sigue un
diagrama de estados muy sencillo. Cada uno de los saltos del diagrama de
estados se comprueba cada 2016 bloques. Si el estado pasa a *locked-in*,
significa que está aceptado y que se utilizará a partir del siguiente ciclo de
2016 bloques.

> NOTA: Este BIP impone un mínimo del 95% de aceptación (*threshold*). Al ser
> cambios globales, se pretende que la mayor parte de usuarios esté de acuerdo
> con los cambios para evitar que se centralice el criterio.

![Diagrama de estados del BIP-39, tomado del propio BIP](img/bip-0009_states.png)

De cara a presentar cada nuevos soft forks a aplicar siguiendo este BIP, el BIP
fuerza a utilizar varios valores que definen el comportamiento del diagrama de
estados y el bit de versión a utilizar. De este modo, cada nuevo cambio sigue
un proceso de aceptación controlado.

## Aplicaciones sobre Bitcoin

Bitcoin no está definido con parámetros demasiado específicos de su sector o de
la solución que implemente como podrían ser: balances, usuarios, etc. Sino que
parte de otros puntos y abstrae sobre ellos: scripts, blockchain, claves,
direcciones, transacciones (*input* y *output*). Por tanto, cualquier
redefinición del comportamiento de estos puntos puede dar pie a la
implementación de otras tecnologías.

### ¿Qué aporta Bitcoin?

- Resistencia a doble gasto
- Inmutabilidad
- Neutralidad
- Sello temporal seguro
- Autorización
- Auditabilidad
- Contabilidad
- No expiración
- Integridad
- Atomicidad
- Unidades de valor discreto (indivisible)
- Quorum de control
- Timelocks
- Replicación
- *Forgery protection*
- Consistencia
- Guardado de estado externo
- Cantidad predecible de moneda

Estos puntos permiten crear nuevas aplicaciones combinando los ingredientes
necesarios. Por ejemplo:

- Notaría digital. Concepto *Proof-Of-Existance*
    - Inmutabilidad
    - Sello temporal seguro
    - Durabilidad

- Aplicaciones de crowdfunding
    - Consistencia
    - Atomicidad
    - Integridad

- *Payment Channels*. Envío de dinero *off-chain*
    - Quorum de control
    - Sello temporal seguro
    - Resistencia a doble gasto
    - No expiración
    - Neutralidad
    - Autorización

### Aplicaciones sobre el Blockchain de Bitcoin

Existen aplicaciones que introduciendo metadatos en el Blockchain de Bitcoin
extienden su comportamiento.

1. **Colored Coins**: Es una forma de "pintar bitcoins" como quien escribe en
   un billete para asegurar la autenticidad de alguna operación.
    - Hacen uso de sentencias tipo `OP_RETURN`
    - Necesitan carteras y exploradores específicos para su propio protocolo ya
      que necesitan introducir metadatos y entenderlos.
    - [Más información](https://es.wikipedia.org/wiki/Moneda_coloreada)

> NOTA: las carteras no-coloreadas pueden pisar los metadatos introducidos
> sobre monedas coloreadas.

2. **Counterparty**: Counterparty es un ejemplo de protocolo sobre la red de
   Bitcoin.
    - Hace uso de sentencias tipo `OP_RETURN` o introduce los metadatos
      asociados en el campo de clave pública de direcciones *multisig*.
    - Permite crear nuevas criptomonedas, contratos inteligentes avanzados,
      etc.
    - [Más información](https://counterparty.io/)

> NOTA: Como ejemplo de aplicación real, Storj funcionaba en su primera versión
> sobre Counterparty, más adelante migró a una criptomoneda que cumplía el
> estándar ERC-20 de Ethereum. La migración se realizó con la intención de
> reducir las tasas y tener más flexibilidad en los contratos inteligentes.
>
> [https://storj.io/blog/2017/03/migration-from-counterparty-to-ethereum/](https://storj.io/blog/2017/03/migration-from-counterparty-to-ethereum/).
