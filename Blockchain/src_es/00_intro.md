---
author:         ElenQ Technology
title:          Ayuntamiento de Vitoria
subtitle:       Introducción al Blockchain
license:        CC-BY-SA 4.0
changes:        false
changestitle:   Revision history
toc:            true
documentclass:  book
lang:           spanish
links-as-notes: true # Easy printing
---


# Licencia

Este documento se publica bajo licencia Creative Commons Attributtion
Share-Alike 4.0.
